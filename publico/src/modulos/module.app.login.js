var login = angular.module('login', [
    'ui.router',
    'module.directives',
    'module.helpers',
    'ui.utils.masks',
    'ngMaterial',
    'usuario-module'
]);

login.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('myHttpInterceptor');
}]);

//registra um interceptor como um serviço
login.factory('myHttpInterceptor', ['$q', 'Mensagens', function ($q, Mensagens) {

    return {
        // optional method
        'request': function (config) {
            return config;
        },

        // optional method
        'requestError': function (rejection) {
            return $q.reject(rejection);
        },

        // optional method
        'response': function (response) {
            return response;
        },

        //request com erros
        'responseError': function (rejection) {

            var deferred = $q.defer();
            Mensagens.limparErros();

            // error 400 Bad Request
            if (rejection.status === 400) {
                if (rejection.data && rejection.data.tipo_erro == "erro_de_negocio") {
                    Mensagens.mostraErros([rejection.data.errors]);
                    return $q(function () {
                    });
                }
            }

            // error 401 Unauthorized
            if (rejection.status === 401) {

            }

            if (rejection.status === 500) {
                if (rejection.data && rejection.data.tipo_erro == "erro_de_negocio") {
                    Mensagens.mostraErros([rejection.data.errors]);
                    return $q(function () {
                    });
                }
            }

            return $q.reject(rejection); // not a recoverable error
        }
    };
}]);

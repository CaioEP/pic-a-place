"use strict";

window['TAMANHO_PAGINA'] = 10;
window['TEMPO_MENSAGEM'] = 10000;

var atendimentoApp = angular.module('app', [
    'ui.router',
    'module.directives',
    'module.helpers',
    'ui.utils.masks',
    'ngFileUpload',
    'seguranca',
    'ngMaterial',
    'md.data.table',
    'detalhar-imagem.module',
    'usuario-module',
    'ngMap'
]);


atendimentoApp.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('myHttpInterceptor');
}]);

atendimentoApp.config(function ($mdThemingProvider) {
    $mdThemingProvider.theme('default')
        .primaryPalette('purple') // #31389a
        .accentPalette('pink');   // #315595
});

//registra um interceptor como um serviço
atendimentoApp.factory('myHttpInterceptor', ['$q', 'Mensagens', function ($q, Mensagens) {

    return {
        // optional method
        'request': function (config) {
            return config;
        },

        // optional method
        'requestError': function (rejection) {
            return $q.reject(rejection);
        },

        // optional method
        'response': function (response) {
            return response;
        },

        //request com erros
        'responseError': function (rejection) {

            const deferred = $q.defer();
            Mensagens.limparErros();

            // error 400 Bad Request
            if (rejection.status === 400) {
                if (rejection.data && rejection.data.tipo_erro == "erro_de_negocio") {
                    Mensagens.mostraErros([rejection.data.errors]);
                    return $q.reject(rejection);
                }
            }

            // error 401 Unauthorized
            if (rejection.status === 401) {
                if (rejection.data && rejection.data.tipo_erro == "erro_de_autenticacao") {
                    $('a').addClass('link-disabled');
                    bootbox.alert(rejection.data.errors, function () {
                        $('a').removeClass('link-disabled');
                        window.location.href = '/login.html';
                    });
                    return $q(function () {
                    });
                }
            }

            if (rejection.status === 403) {
                if (rejection.data && rejection.data.tipo_erro == "erro_de_permissao") {
                    $('a').addClass('link-disabled');
                    alert('Você não tem permissão para acessar a funcionalidade!');
                    $('a').removeClass('link-disabled');
                    window.location.href = '/';
                    return $q(function () {
                    });
                }
            }

            if (rejection.status === 500) {
                if (rejection.data && rejection.data.tipo_erro == "erro_de_negocio") {
                    Mensagens.mostraErros([rejection.data.errors]);
                    return $q(function () {
                    });
                }
            }


            return $q.reject(rejection); // not a recoverable error
        }
    };
}]);

//adiciona os estados raizes do sistema
//esses estados são chamados via menu, portanto podem resetar a pilha
atendimentoApp.run(['PilhaDeNavegacao', function (PilhaDeNavegacao) {
    PilhaDeNavegacao
        .raiz('inicio')
        .raiz('usuario-listar')
}]);


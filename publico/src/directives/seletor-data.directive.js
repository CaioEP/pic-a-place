"use strict";

(function () {

    'use strict';

    var DATEPICKER_OPTS = {
        format: 'dd/mm/yyyy',
        language: 'pt-BR',
        weekStart: 0,
        disableTouchKeyboard: true,
        forceParse: false,
        todayHighlight: true,
        autoclose: true
    };

    var module = angular.module('module.directives');

    module.directive('simplesSeletorData', ['$filter', function ($filter) {
        return {
            require: 'ngModel',
            replace: true,
            link: function (scope, element, attrs, ngModelCtrl) {

                var $elm = $(element);

                scope.$watch(function () {
                    return ngModelCtrl.$modelValue;
                }, function () {
                    //$elm.datepicker('update');
                });

                var opts = angular.extend({}, DATEPICKER_OPTS, {
                    startDate: attrs['simplesDataInicial'],
                    endDate: attrs['simplesDataFinal']
                });

                function triggerHandler() {
                    angular.element(element).triggerHandler('input');
                }

                var mask = new StringMask('99/99/9999');
                $elm
                    .on('changeDate', triggerHandler)
                    .attr('maxlength', 10)
                    .datepicker(opts);
                mask.apply($elm)

                if (attrs['simplesReferenciaInicial']) {

                    $(attrs['simplesReferenciaInicial']).on('changeDate', function (e) {

                        $elm.datepicker('setStartDate', e.date);

                        var dataMinima = $(this).datepicker('getDate');
                        var dataAtual = $elm.datepicker('getDate');

                        if (dataAtual != null && dataMinima > dataAtual) {
                            $elm.datepicker('setDate', dataMinima);
                        }

                    });

                }

                if (attrs['simplesReferenciaFinal']) {

                    $(attrs['simplesReferenciaFinal']).on('changeDate', function (e) {

                        $elm.datepicker('setEndDate', e.date);

                        var dataMaxima = $(this).datepicker('getDate');
                        var dataAtual = $elm.datepicker('getDate');

                        if (dataAtual != null && dataMaxima < dataAtual) {
                            $elm.datepicker('setDate', dataMaxima);
                        }

                    });

                }

                ngModelCtrl.$formatters.unshift(function (value) {
                    return $filter('date')(value, 'dd/MM/yyyy');
                });

                ngModelCtrl.$parsers.push(function (value) {
                    return value === "" ? null : value;
                });

            }
        };
    }]);

})();
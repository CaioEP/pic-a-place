"use strict";

angular.module("module.directives")

.directive('simplesTable', function () {
    return {
        restrict: 'A',
        scope: {
            simplesTableOptions: '='
        },

        controller: ['$scope', function ($scope) {

            var ths = $scope.ths = [];

            this.addTh = function (th) {               
                ths.push(th);
            };

            this.sort = function (campoSort, sortDir, element) {

                angular.forEach(ths, function (th) {
                    if (element != th) {
                        th.find('#icon-ordenacao').remove();
                    }
                });

                if (!$scope.simplesTableOptions) {
                    alert('Para efetuar uma ordena��o o atributo simplesTableOptions deve ser implementado na table');
                    return;
                }

                if (!$scope.simplesTableOptions.ordenarPor) {
                    alert('Para efetuar uma ordena��o a fun��o ordenarPor deve ser implementado no atributo simples-table-options');
                    return;
                }

                $scope.simplesTableOptions.ordenarPor({
                    campoSort: campoSort
                  , sortDir: sortDir
                });
            };
        }]

    };
})

.directive('simplesSort', function () {

    function link(scope, element, attrs, tableCtrl) {

        tableCtrl.addTh(element);

        element.addClass('simples-sort');
        
        element.on('click', function (event) {
            scope.sortAsc = !scope.sortAsc;

            var sortDir = scope.sortAsc ? 'asc' : 'desc'

            var icon = angular.element('<i id="icon-ordenacao" class="fa simples-table-sort-' + sortDir + '"></i>');

            element.find('#icon-ordenacao').remove();
            element.append(icon);

            tableCtrl.sort(attrs.simplesSort, sortDir, element);
        });
    };
    
    return {
        require: '^^simplesTable',
        restrict: 'A',
        link: link,
        scope: {}
    };
});
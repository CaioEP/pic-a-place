"use strict";

var module = angular.module('module.directives');

module.directive('somenteNumeros', function () {
    return {
        require: 'ngModel',
       
        link: function(scope, element, attrs, modelCtrl) {

        	var attr = attrs.somenteNumeros || '{}';
        	
        	var optionsDefault = { permitirNegativo: false };
        	var options = angular.extend(optionsDefault, eval("(" + attr + ")"));
        	
            modelCtrl.$parsers.push(function (inputValue) {
            	
            	var transformedInput = inputValue ? inputValue.replace(options.permitirNegativo ? /[^\d.-]/g : /[^\d]/g,'') : null;

                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});
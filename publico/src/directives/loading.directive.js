angular.module("module.directives").directive('loading',
    ['$http', function ($http) {
        return {
            restrict: 'A',
            link: function (scope, elm, attrs) {
                scope.isLoading = function () {
                    var requests = $http.pendingRequests;
                    var retorno = false;
                    requests.forEach(function (request) {
                        retorno = !request.desabilitarLoading;
                    });
                    return retorno && $http.pendingRequests.length > 0;
                };

                scope.$watch(scope.isLoading, function (v) {
                    var $conteudo = $('#conteudo');
                    if (v) {
                        $conteudo.css('-webkit-filter', 'blur(2px)');
                        $conteudo.css('cursor', 'wait');
                        $conteudo.css('pointer-events', 'none');
                        elm.show();
                    } else {
                        $conteudo.css('-webkit-filter', 'blur(0px)');
                        $conteudo.css('cursor', 'default');
                        $conteudo.css('pointer-events', 'auto');
                        elm.hide();
                    }
                });
            }
        };

    }]);
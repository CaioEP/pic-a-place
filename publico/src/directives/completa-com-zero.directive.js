"use strict";

var module = angular.module("module.directives");

module.directive('completaComZero', function () {

    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {

            element.bind('blur', function () {
            	
            	var attr = attrs.completaComZero || '{}';
            	var optionsDefault = { preencherQuandoVazio: true };
            	var options = angular.extend(optionsDefault, eval("(" + attr + ")"));

                var numero = parseInt(modelCtrl.$viewValue, 10);
                var tamanho = parseInt(attrs.completaComZero, 10);
                
                var preencher = function(numero, tamanho){
                	numero = '' + numero;
                    while (numero.length < tamanho) {
                        numero = '0' + numero;
                    }
                    modelCtrl.$setViewValue(numero);
                    modelCtrl.$render();
                }
                
                if (isNaN(numero) || isNaN(tamanho)) {
                	 if(options.preencherQuandoVazio == true){
                		 preencher(0, tamanho);
                	 }
                }else{
                	preencher(numero, tamanho);
                }
            });
        }
    }
});
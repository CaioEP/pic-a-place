"use strict";

var module = angular.module("module.directives");

module.directive("simplesPaginacao", function () {
	return {
		scope: {
			paginaDeRegistros: '=',
			pagina_onClick: '=paginaOnClick',
			pagina: '=',
			registrosPorPagina: '='
		},
		controller: function ($scope) {

			$scope.getTotalDePaginas = function (totalDeRegistros) {
				return Math.ceil(totalDeRegistros / $scope.registrosPorPagina);
			};

			$scope.getRangeDePaginas = function (totalDeRegistros) {
				var rangeDePaginas = new Array();
				var pagina = $scope.pagina;
				var paginaInicial = Math.max(1, pagina - 4);
				var paginaFinal = Math.max(1, Math.min($scope.getTotalDePaginas(totalDeRegistros), pagina + 4));
				while (paginaFinal - paginaInicial > 4) {
					if (paginaFinal - pagina >= pagina - paginaInicial) {
						paginaFinal--;
					} else {
						paginaInicial++;
					}
				}
				for (var i = paginaInicial; i <= paginaFinal; i++) {
					rangeDePaginas.push(i);
				}
				return rangeDePaginas;
			};

		},
		templateUrl: '/src/directives/paginacao.html'
	};
});

var moduloSeguranca = angular.module('seguranca', []);

moduloSeguranca.directive('requerRole', ['$http', function ($http) {

    var roles = [];

    $http({
        url: '/api/usuario/roles',
        method: 'Get'
    }).then(function (result) {
        let data = result.data;
        if (!data) {
            window.location.href = 'login.html';
        }
        for (var i = 0; i < data.length; i++) {
            roles.push(data[i]);
        }
    });

    //pode receber uma role ou várias separadas por |
    function possuiRole(r, roles) {
        var rolesSplit = r.split('|');

        for (var i = 0; i < rolesSplit.length; i++) {

            for (var j = 0; j < roles.length; j++) {
                if (roles[j] == rolesSplit[i]) {//caso encontre o laço pode ser parado
                    return true;//efetua o break
                }
            }

        }

        return false;
    }

    return {
        link: function (scope, elm, attrs) {

            scope.roles = roles;

            scope.$watchCollection('roles', function (roles) {

                if (!possuiRole(attrs.requerRole, roles)) {
                    elm.hide();
                } else {
                    elm.show();
                }
            });

        }
    };

}]);
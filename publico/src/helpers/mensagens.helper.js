"use strict";

let mensagens = ['$timeout', '$interval', '$window', function ($timeout, $interval, $window) {

    let thisObj = {
        erros: [],
        sucesso: null,
        sucessoPreRoute: null
    };

    thisObj.mostraErros = function (mensagens, suffix) {
        let erros = [];
        for (let index in mensagens) {
            erros.push({texto: mensagens[index], surgimento: new Date().getTime()});
        }
        if (!suffix) {
            thisObj.erros = erros;
        } else {
            thisObj["erros" + suffix] = erros;
        }
        if (erros.length > 0) {
            thisObj.sucesso = null;
            $window.scrollTo(0, 0);
        }
    };

    thisObj.mostraSucesso = function (mensagem) {
        this.erros = [];
        thisObj.sucesso = mensagem;
        $window.scrollTo(0, 0);
    };

    thisObj.limparErros = function () {
        this.erros = [];
    };

    thisObj.limparSucesso = function () {
        this.sucesso = null;
    };

    thisObj.limpar = function () {
        this.sucesso = null;
        this.erros = [];
    };

    return thisObj;

}];

angular.module('module.helpers').factory('Mensagens', mensagens);

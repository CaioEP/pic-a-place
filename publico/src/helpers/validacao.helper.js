'use strict';

var module = angular.module("module.helpers");

module.factory('ValidacaoHelper', ['$http', 'Mensagens', function ($http, Mensagens) {

    return {
        onError: function (rejection) {
            Mensagens.limpar();
            if (rejection.data.tipo_erro && rejection.data.tipo_erro == "erro_de_validacao") {
                Mensagens.mostraErros(['Existem campos obrigatórios não preenchidos']);
            } else if (rejection.data.tipo_erro && rejection.data.tipo_erro == "erro_de_negocio") {
                Mensagens.mostraErros([rejection.data.errors]);
            } else {
                //outros tipos de erro não tratado
                Mensagens.mostraErros(["Ocorreram problemas não tratados pelo sistema."]);
            }
        }
    };
}]);
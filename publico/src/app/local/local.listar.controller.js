'use strict';

angular.module('app').controller('LocalListarController',
    ['$scope', '$state', '$stateParams', 'LocalService', 'ValidacaoHelper', 'Mensagens', 'PilhaDeNavegacao', '$mdBottomSheet',
        function ($scope, $state, $stateParams, LocalService, ValidacaoHelper, Mensagens, PilhaDeNavegacao, $mdBottomSheet) {


            _criarFiltro();
            $scope.filtroAplicado = angular.copy($scope.filtro);
            _consultar();

            $scope.abrirMenu = function (event, local) {
                Mensagens.limpar();
                $mdBottomSheet.show({
                    templateUrl: '/src/app/local/menu-local.html',
                    controller: 'MenuLocalController',
                    clickOutsideToClose: true,
                    locals: {
                        idLocal: local.id
                    }
                }).then(function (acao) {
                    Mensagens.mostraSucesso('Local ' + (acao == 'editar' ? 'alterado' : 'excluído') + ' com sucesso');
                    _consultar();
                }).catch(function (error) {
                    if (error)
                        Mensagens.mostraErros([error.message]);
                });
            };

            function _consultar() {
                $scope.filtroAplicado = angular.copy($scope.filtro);
                LocalService.consultar($scope.filtroAplicado).then(result => {
                    if (result.status == 200) {
                        $scope.paginaDeRegistros = result.data;
                    }
                }, ValidacaoHelper.onError);
            }

            function _criarFiltro() {
                $scope.filtro = {
                    pagina: 1,
                    tamanhoDaPagina: 100//window['TAMANHO_PAGINA']
                };
            }

        }]);
"use strict";

angular.module('app').run(['PilhaDeNavegacao', function (PilhaDeNavegacao) {

    PilhaDeNavegacao
        .produtor('local-listar', [])
        .produtor('local-cadastrar', []);

}]);

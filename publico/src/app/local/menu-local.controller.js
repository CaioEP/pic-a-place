'use strict';

angular.module('app').controller('MenuLocalController',
    ['$scope', '$mdBottomSheet', '$state', 'ValidacaoHelper', '$mdDialog', 'LocalService', 'idLocal',
        function ($scope, $mdBottomSheet, $state, ValidacaoHelper, $mdDialog, LocalService, idLocal) {

            $scope.editar = function () {
                $mdBottomSheet.hide('editar');
                $state.go('local-cadastrar', {id: idLocal})
            };

            $scope.excluir = function (event) {
                let confirm = $mdDialog.confirm()
                    .title('Você deseja excluir este local?')
                    .textContent('Todos os seus dados serão excluídos do sistema')
                    .targetEvent(event)
                    .ok('Sim')
                    .cancel('Não');

                $mdDialog.show(confirm).then(function () {
                    LocalService.excluir(idLocal).then(() => {
                        $mdBottomSheet.hide('excluir');
                    }, ValidacaoHelper.onError);
                });

            };

            $scope.verFotos = function (event) {
                LocalService.buscarLocalPorId(idLocal).then(result => {
                    if (result.status == 200) {
                        let obj = {
                            url: 'https://upload.wikimedia.org/wikipedia/commons/a/a3/Capivaras_na_USP_2.jpg',
                            descricao: 'Descrição da Foto'
                        };
                    }
                });
            }

        }]);
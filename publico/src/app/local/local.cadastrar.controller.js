'use strict';

angular.module('app').controller('LocalCadastrarController',
    ['$scope', '$state', '$stateParams', 'LocalService', 'ValidacaoHelper', 'Mensagens', 'PilhaDeNavegacao', 'NgMap', 'toastAlert',
        function ($scope, $state, $stateParams, LocalService, ValidacaoHelper, Mensagens, PilhaDeNavegacao, NgMap, toastAlert) {

            $scope.id = PilhaDeNavegacao.getOpcao('id', null);
            var retornando = PilhaDeNavegacao.recuperar($scope, function (produtor, valor) {
                switch (produtor.estado) {
                    case 'local-listar':
                        $state.go('usuario-cadastrar', {id: valor});
                    default:
                        break;
                }
            });

            if (!$scope.id) {
                $scope.id = $stateParams.id;
            }

            _initMap();
            _carregarTiposPropriedades();
            $scope.voltar = _voltar;

            function _carregarTiposPropriedades() {
                LocalService.buscarTiposPropriedade().then(result => {
                    $scope.tipos_propriedade = result.data;
                    _criarModel();
                    _buscarLocalPorId();
                });
            }

            $scope.zerarValorEntrada = function () {
                if ($scope.model.id_tipo_propriedade == 1) {
                    $scope.model.valor_entrada = 0.0;
                }
            };

            $scope.salvar = function () {
                if (!_camposValidos()) {
                    toastAlert.defaultToaster('Clique no mapa para marcar a localização');
                    return;
                }

                if (!$scope.model.id) {
                    LocalService.salvar($scope.model).then(result => {
                        if (result.status = 200) {
                            Mensagens.sucessoPreRoute = "Novo Local criado!";
                            _voltar();
                        }
                    }, ValidacaoHelper.onError);
                } else {
                    LocalService.alterar($scope.model.id, $scope.model).then(result => {
                        if (result.status = 200) {
                            Mensagens.sucessoPreRoute = "Local alterado!";
                            _voltar();
                        }
                    }, ValidacaoHelper.onError);
                }
            };

            function _buscarLocalPorId() {
                let id = $scope.id;
                if (id) {
                    LocalService.buscarLocalPorId(id).then(result => {
                        if (result.status == 200) {
                            $scope.model = result.data;
                        }
                    }, ValidacaoHelper.onError);
                }
            }

            function _voltar() {
                let retornou = PilhaDeNavegacao.retornar();
                if (!retornou) {
                    $state.go('local-listar');
                }
            }

            function _camposValidos() {
                let model = $scope.model;
                return model.latitude && model.longitude;
            }

            function _initMap() {
                NgMap.getMap();
            }

            $scope.adicionarPino = function (event) {
                let ll = event.latLng;
                $scope.pino = {
                    pos: [ll.lat(), ll.lng()]
                };
                $scope.model.latitude = ll.lat();
                $scope.model.longitude = ll.lng();
            };

            function _criarModel() {
                $scope.model = {
                    id_tipo_propriedade: '1',
                    nome: null,
                    latitude: null,
                    longitude: null,
                    valor_entrada: 0.00
                };
            }


        }]);

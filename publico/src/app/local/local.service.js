'use strict';

angular.module('app').factory('LocalService',
    ['$http', function ($http) {

        return {
            buscarTiposPropriedade: function () {
                return $http({
                    method: 'GET',
                    url: '/api/local/tipos_propriedade'
                });
            },

            buscarLocalPorId: function (id) {
                return $http({
                    method: 'GET',
                    url: '/api/local/' + id,
                });
            },

            salvar: function (local) {
                return $http({
                    method: 'POST',
                    url: '/api/local/salvar',
                    data: local
                });
            },

            alterar: function (id, local) {
                return $http({
                    method: 'PUT',
                    url: '/api/local/alterar/' + id,
                    data: local
                });
            },

            consultar: function (filtro) {
                return $http({
                    method: 'GET',
                    url: '/api/local',
                    params: filtro
                });
            },

            excluir: function (id) {
                return $http({
                    method: 'DELETE',
                    url: '/api/local/' + id
                });
            }
        };
    }]);
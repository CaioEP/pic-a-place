'use strict';

angular.module('app').config(['$stateProvider', function ($stateProvider) {
    $stateProvider
        .state('local-listar', {
            url: "/local/listar",
            templateUrl: "src/app/local/local.listar.html",
            controller: "LocalListarController"
        })
        .state('local-cadastrar', {
            url: "/local/cadastrar/:id",
            templateUrl: "src/app/local/local.cadastrar.html",
            controller: "LocalCadastrarController"
        });
}]);


'use strict';

angular.module('app').controller('MenuTabelaUsuerioController',
    ['$scope', '$mdBottomSheet', '$state', 'ValidacaoHelper', '$mdDialog', 'UsuarioService', 'idUsuario',
        function ($scope, $mdBottomSheet, $state, ValidacaoHelper, $mdDialog, UsuarioService, idUsuario) {

            $scope.editar = function (ev) {
                $mdDialog.show({
                    controller: 'UsuarioCadastrarController',
                    templateUrl: '/src/compartilhado/usuario/usuario.cadastrar.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: true,
                    locals: {
                        idUsuario: idUsuario,
                        exibePerfil: true
                    }
                }).then(function (usuario) {
                    if (usuario)
                        $mdBottomSheet.hide('editar');
                });
            };

            $scope.excluir = function ($event) {
                let confirm = $mdDialog.confirm()
                    .title('Você deseja excluir este usuário?')
                    .textContent('Todos os seus dados serão excluídos do sistema')
                    .targetEvent($event)
                    .ok('Sim')
                    .cancel('Não');

                $mdDialog.show(confirm).then(function () {
                    UsuarioService.excluir(idUsuario).then(() => {
                        $mdBottomSheet.hide('excluir');
                    }, ValidacaoHelper.onError);
                });

            };

        }]);
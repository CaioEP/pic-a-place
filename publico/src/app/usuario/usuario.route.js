'use strict';

angular.module('app').config([ '$stateProvider', function ($stateProvider) {
	$stateProvider
    	.state('usuario-listar', {
    		url: "/usuario/listar",
    		templateUrl: "src/app/usuario/usuario.listar.html",
    		controller: "UsuarioListarController"
    	});
}]);


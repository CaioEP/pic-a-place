'use strict';

angular.module('app').controller('UsuarioListarController',
    ['$scope', '$state', '$stateParams', 'UsuarioService', 'ValidacaoHelper', 'Mensagens', 'PilhaDeNavegacao', '$mdDialog', '$mdBottomSheet',
        function ($scope, $state, $stateParams, UsuarioService, ValidacaoHelper, Mensagens, PilhaDeNavegacao, $mdDialog, $mdBottomSheet) {

            PilhaDeNavegacao.recuperar($scope, function (produtor, valor) {
            });

            $scope.paginaDeRegistros = {};
            _criarFiltro();
            $scope.filtroAplicado = angular.copy($scope.model);
            _consultar();

            $scope.consultar = function () {
                $scope.filtroAplicado = angular.copy($scope.model);
                _consultar();
            };

            $scope.cadastrar = function (ev) {
                Mensagens.limpar();
                $mdDialog.show({
                    controller: 'UsuarioCadastrarController',
                    templateUrl: '/src/compartilhado/usuario/usuario.cadastrar.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: true,
                    locals: {
                        idUsuario: null,
                        exibePerfil: true
                    }
                }).then(function (usuario) {
                    if (usuario) {
                        Mensagens.mostraSucesso("Usuário salvo com sucesso");
                        $scope.consultar();
                    }
                });
            };

            $scope.trocarDePagina = function (pagina) {
                $scope.filtroAplicado.pagina = pagina;
                _consultar();
            };

            $scope.limparFiltro = function () {
                Mensagens.limpar();
                _criarFiltro();
                $scope.filtroAplicado = angular.copy($scope.model);
                _consultar();
            };

            $scope.abrirMenu = function (id) {
                Mensagens.limpar();
                $mdBottomSheet.show({
                    templateUrl: '/src/app/usuario/menu-tabela-usuario.html',
                    controller: 'MenuTabelaUsuerioController',
                    clickOutsideToClose: true,
                    locals: {
                        idUsuario: id
                    }
                }).then(function (acao) {
                    Mensagens.mostraSucesso('Usuário ' + (acao == 'editar' ? 'alterado' : 'excluído') + ' com sucesso');
                    $scope.consultar();
                }).catch(function (error) {
                    if (error)
                        Mensagens.mostraErros([error.message]);
                });
            };

            function _consultar() {
                UsuarioService.consultar($scope.filtroAplicado).then(lista => {
                    $scope.paginaDeRegistros = lista.data;
                });
            }

            function _criarFiltro() {
                $scope.model = {
                    nome: null,
                    email: null,
                    pagina: 1,
                    tamanhoDaPagina: window['TAMANHO_PAGINA']
                };
            }


        }]);

'use strict';

angular.module('app').config([ '$stateProvider', function ($stateProvider) {
	$stateProvider
    	.state('inicio', {
    		url: "",
    		templateUrl: "src/app/inicio/inicio.html",
    		controller: "InicioController"
    	});
}]);


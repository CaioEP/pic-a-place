'use strict';

angular.module('app').factory('Inicio', ['$http', function ($http) {

    return {

        recuperarUsuarioDaSessao: function () {
            return $http({
                method: 'GET',
                url: '/api/inicio/usuario_sessao'
            });
        },

        sair: function () {
            return $http({
                method: 'GET',
                url: '/api/logout'
            });
        }

    };


}]);

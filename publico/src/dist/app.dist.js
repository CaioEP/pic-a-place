'use strict';

angular.module('app').controller('InicioController',
    ['$scope', function ($scope) {




    }]);
'use strict';

angular.module('app').config([ '$stateProvider', function ($stateProvider) {
	$stateProvider
    	.state('inicio', {
    		url: "",
    		templateUrl: "src/app/inicio/inicio.html",
    		controller: "InicioController"
    	});
}]);


'use strict';

angular.module('app').factory('Inicio', ['$http', function ($http) {

    return {

        recuperarUsuarioDaSessao: function () {
            return $http({
                method: 'GET',
                url: '/api/inicio/usuario_sessao'
            });
        },

        sair: function () {
            return $http({
                method: 'GET',
                url: '/api/logout'
            });
        }

    };


}]);

'use strict';

angular.module('app').controller('LocalCadastrarController',
    ['$scope', '$state', '$stateParams', 'LocalService', 'ValidacaoHelper', 'Mensagens', 'PilhaDeNavegacao', 'NgMap', 'toastAlert',
        function ($scope, $state, $stateParams, LocalService, ValidacaoHelper, Mensagens, PilhaDeNavegacao, NgMap, toastAlert) {

            $scope.id = PilhaDeNavegacao.getOpcao('id', null);
            var retornando = PilhaDeNavegacao.recuperar($scope, function (produtor, valor) {
                switch (produtor.estado) {
                    case 'local-listar':
                        $state.go('usuario-cadastrar', {id: valor});
                    default:
                        break;
                }
            });

            if (!$scope.id) {
                $scope.id = $stateParams.id;
            }

            _initMap();
            _carregarTiposPropriedades();
            $scope.voltar = _voltar;

            function _carregarTiposPropriedades() {
                LocalService.buscarTiposPropriedade().then(result => {
                    $scope.tipos_propriedade = result.data;
                    _criarModel();
                    _buscarLocalPorId();
                });
            }

            $scope.zerarValorEntrada = function () {
                if ($scope.model.id_tipo_propriedade == 1) {
                    $scope.model.valor_entrada = 0.0;
                }
            };

            $scope.salvar = function () {
                if (!_camposValidos()) {
                    toastAlert.defaultToaster('Clique no mapa para marcar a localização');
                    return;
                }

                if (!$scope.model.id) {
                    LocalService.salvar($scope.model).then(result => {
                        if (result.status = 200) {
                            Mensagens.sucessoPreRoute = "Novo Local criado!";
                            _voltar();
                        }
                    }, ValidacaoHelper.onError);
                } else {
                    LocalService.alterar($scope.model.id, $scope.model).then(result => {
                        if (result.status = 200) {
                            Mensagens.sucessoPreRoute = "Local alterado!";
                            _voltar();
                        }
                    }, ValidacaoHelper.onError);
                }
            };

            function _buscarLocalPorId() {
                let id = $scope.id;
                if (id) {
                    LocalService.buscarLocalPorId(id).then(result => {
                        if (result.status == 200) {
                            $scope.model = result.data;
                        }
                    }, ValidacaoHelper.onError);
                }
            }

            function _voltar() {
                let retornou = PilhaDeNavegacao.retornar();
                if (!retornou) {
                    $state.go('local-listar');
                }
            }

            function _camposValidos() {
                let model = $scope.model;
                return model.latitude && model.longitude;
            }

            function _initMap() {
                NgMap.getMap();
            }

            $scope.adicionarPino = function (event) {
                let ll = event.latLng;
                $scope.pino = {
                    pos: [ll.lat(), ll.lng()]
                };
                $scope.model.latitude = ll.lat();
                $scope.model.longitude = ll.lng();
            };

            function _criarModel() {
                $scope.model = {
                    id_tipo_propriedade: '1',
                    nome: null,
                    latitude: null,
                    longitude: null,
                    valor_entrada: 0.00
                };
            }


        }]);

'use strict';

angular.module('app').controller('LocalListarController',
    ['$scope', '$state', '$stateParams', 'LocalService', 'ValidacaoHelper', 'Mensagens', 'PilhaDeNavegacao', '$mdBottomSheet',
        function ($scope, $state, $stateParams, LocalService, ValidacaoHelper, Mensagens, PilhaDeNavegacao, $mdBottomSheet) {


            _criarFiltro();
            $scope.filtroAplicado = angular.copy($scope.filtro);
            _consultar();

            $scope.abrirMenu = function (event, local) {
                Mensagens.limpar();
                $mdBottomSheet.show({
                    templateUrl: '/src/app/local/menu-local.html',
                    controller: 'MenuLocalController',
                    clickOutsideToClose: true,
                    locals: {
                        idLocal: local.id
                    }
                }).then(function (acao) {
                    Mensagens.mostraSucesso('Local ' + (acao == 'editar' ? 'alterado' : 'excluído') + ' com sucesso');
                    _consultar();
                }).catch(function (error) {
                    if (error)
                        Mensagens.mostraErros([error.message]);
                });
            };

            function _consultar() {
                $scope.filtroAplicado = angular.copy($scope.filtro);
                LocalService.consultar($scope.filtroAplicado).then(result => {
                    if (result.status == 200) {
                        $scope.paginaDeRegistros = result.data;
                    }
                }, ValidacaoHelper.onError);
            }

            function _criarFiltro() {
                $scope.filtro = {
                    pagina: 1,
                    tamanhoDaPagina: 100//window['TAMANHO_PAGINA']
                };
            }

        }]);
"use strict";

angular.module('app').run(['PilhaDeNavegacao', function (PilhaDeNavegacao) {

    PilhaDeNavegacao
        .produtor('local-listar', [])
        .produtor('local-cadastrar', []);

}]);

'use strict';

angular.module('app').config(['$stateProvider', function ($stateProvider) {
    $stateProvider
        .state('local-listar', {
            url: "/local/listar",
            templateUrl: "src/app/local/local.listar.html",
            controller: "LocalListarController"
        })
        .state('local-cadastrar', {
            url: "/local/cadastrar/:id",
            templateUrl: "src/app/local/local.cadastrar.html",
            controller: "LocalCadastrarController"
        });
}]);


'use strict';

angular.module('app').factory('LocalService',
    ['$http', function ($http) {

        return {
            buscarTiposPropriedade: function () {
                return $http({
                    method: 'GET',
                    url: '/api/local/tipos_propriedade'
                });
            },

            buscarLocalPorId: function (id) {
                return $http({
                    method: 'GET',
                    url: '/api/local/' + id,
                });
            },

            salvar: function (local) {
                return $http({
                    method: 'POST',
                    url: '/api/local/salvar',
                    data: local
                });
            },

            alterar: function (id, local) {
                return $http({
                    method: 'PUT',
                    url: '/api/local/alterar/' + id,
                    data: local
                });
            },

            consultar: function (filtro) {
                return $http({
                    method: 'GET',
                    url: '/api/local',
                    params: filtro
                });
            },

            excluir: function (id) {
                return $http({
                    method: 'DELETE',
                    url: '/api/local/' + id
                });
            }
        };
    }]);
'use strict';

angular.module('app').controller('MenuLocalController',
    ['$scope', '$mdBottomSheet', '$state', 'ValidacaoHelper', '$mdDialog', 'LocalService', 'idLocal',
        function ($scope, $mdBottomSheet, $state, ValidacaoHelper, $mdDialog, LocalService, idLocal) {

            $scope.editar = function () {
                $mdBottomSheet.hide('editar');
                $state.go('local-cadastrar', {id: idLocal})
            };

            $scope.excluir = function (event) {
                let confirm = $mdDialog.confirm()
                    .title('Você deseja excluir este local?')
                    .textContent('Todos os seus dados serão excluídos do sistema')
                    .targetEvent(event)
                    .ok('Sim')
                    .cancel('Não');

                $mdDialog.show(confirm).then(function () {
                    LocalService.excluir(idLocal).then(() => {
                        $mdBottomSheet.hide('excluir');
                    }, ValidacaoHelper.onError);
                });

            };

            $scope.verFotos = function (event) {
                alert('Não implementado');
            }

        }]);
'use strict';

angular.module('app').controller('MenuTabelaUsuerioController',
    ['$scope', '$mdBottomSheet', '$state', 'ValidacaoHelper', '$mdDialog', 'UsuarioService', 'idUsuario',
        function ($scope, $mdBottomSheet, $state, ValidacaoHelper, $mdDialog, UsuarioService, idUsuario) {

            $scope.editar = function (ev) {
                $mdDialog.show({
                    controller: 'UsuarioCadastrarController',
                    templateUrl: '/src/compartilhado/usuario/usuario.cadastrar.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: true,
                    locals: {
                        idUsuario: idUsuario,
                        exibePerfil: true
                    }
                }).then(function (usuario) {
                    if (usuario)
                        $mdBottomSheet.hide('editar');
                });
            };

            $scope.excluir = function ($event) {
                let confirm = $mdDialog.confirm()
                    .title('Você deseja excluir este usuário?')
                    .textContent('Todos os seus dados serão excluídos do sistema')
                    .targetEvent($event)
                    .ok('Sim')
                    .cancel('Não');

                $mdDialog.show(confirm).then(function () {
                    UsuarioService.excluir(idUsuario).then(() => {
                        $mdBottomSheet.hide('excluir');
                    }, ValidacaoHelper.onError);
                });

            };

        }]);
'use strict';

angular.module('app').controller('UsuarioListarController',
    ['$scope', '$state', '$stateParams', 'UsuarioService', 'ValidacaoHelper', 'Mensagens', 'PilhaDeNavegacao', '$mdDialog', '$mdBottomSheet',
        function ($scope, $state, $stateParams, UsuarioService, ValidacaoHelper, Mensagens, PilhaDeNavegacao, $mdDialog, $mdBottomSheet) {

            PilhaDeNavegacao.recuperar($scope, function (produtor, valor) {
            });

            $scope.paginaDeRegistros = {};
            _criarFiltro();
            $scope.filtroAplicado = angular.copy($scope.model);
            _consultar();

            $scope.consultar = function () {
                $scope.filtroAplicado = angular.copy($scope.model);
                _consultar();
            };

            $scope.cadastrar = function (ev) {
                Mensagens.limpar();
                $mdDialog.show({
                    controller: 'UsuarioCadastrarController',
                    templateUrl: '/src/compartilhado/usuario/usuario.cadastrar.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: true,
                    locals: {
                        idUsuario: null,
                        exibePerfil: true
                    }
                }).then(function (usuario) {
                    if (usuario) {
                        Mensagens.mostraSucesso("Usuário salvo com sucesso");
                        $scope.consultar();
                    }
                });
            };

            $scope.trocarDePagina = function (pagina) {
                $scope.filtroAplicado.pagina = pagina;
                _consultar();
            };

            $scope.limparFiltro = function () {
                Mensagens.limpar();
                _criarFiltro();
                $scope.filtroAplicado = angular.copy($scope.model);
                _consultar();
            };

            $scope.abrirMenu = function (id) {
                Mensagens.limpar();
                $mdBottomSheet.show({
                    templateUrl: '/src/app/usuario/menu-tabela-usuario.html',
                    controller: 'MenuTabelaUsuerioController',
                    clickOutsideToClose: true,
                    locals: {
                        idUsuario: id
                    }
                }).then(function (acao) {
                    Mensagens.mostraSucesso('Usuário ' + (acao == 'editar' ? 'alterado' : 'excluído') + ' com sucesso');
                    $scope.consultar();
                }).catch(function (error) {
                    if (error)
                        Mensagens.mostraErros([error.message]);
                });
            };

            function _consultar() {
                UsuarioService.consultar($scope.filtroAplicado).then(lista => {
                    $scope.paginaDeRegistros = lista.data;
                });
            }

            function _criarFiltro() {
                $scope.model = {
                    nome: null,
                    email: null,
                    pagina: 1,
                    tamanhoDaPagina: window['TAMANHO_PAGINA']
                };
            }


        }]);

"use strict";

angular.module('app').run([ 'PilhaDeNavegacao', function (PilhaDeNavegacao) {

	PilhaDeNavegacao
		.produtor('usuario-listar', []);

}]);

'use strict';

angular.module('app').config([ '$stateProvider', function ($stateProvider) {
	$stateProvider
    	.state('usuario-listar', {
    		url: "/usuario/listar",
    		templateUrl: "src/app/usuario/usuario.listar.html",
    		controller: "UsuarioListarController"
    	});
}]);


"use strict";

var module = angular.module("module.directives");

module.directive('completaComZero', function () {

    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {

            element.bind('blur', function () {
            	
            	var attr = attrs.completaComZero || '{}';
            	var optionsDefault = { preencherQuandoVazio: true };
            	var options = angular.extend(optionsDefault, eval("(" + attr + ")"));

                var numero = parseInt(modelCtrl.$viewValue, 10);
                var tamanho = parseInt(attrs.completaComZero, 10);
                
                var preencher = function(numero, tamanho){
                	numero = '' + numero;
                    while (numero.length < tamanho) {
                        numero = '0' + numero;
                    }
                    modelCtrl.$setViewValue(numero);
                    modelCtrl.$render();
                }
                
                if (isNaN(numero) || isNaN(tamanho)) {
                	 if(options.preencherQuandoVazio == true){
                		 preencher(0, tamanho);
                	 }
                }else{
                	preencher(numero, tamanho);
                }
            });
        }
    }
});
angular.module("module.directives").directive('loading',
    ['$http', function ($http) {
        return {
            restrict: 'A',
            link: function (scope, elm, attrs) {
                scope.isLoading = function () {
                    var requests = $http.pendingRequests;
                    var retorno = false;
                    requests.forEach(function (request) {
                        retorno = !request.desabilitarLoading;
                    });
                    return retorno && $http.pendingRequests.length > 0;
                };

                scope.$watch(scope.isLoading, function (v) {
                    var $conteudo = $('#conteudo');
                    if (v) {
                        $conteudo.css('-webkit-filter', 'blur(2px)');
                        $conteudo.css('cursor', 'wait');
                        $conteudo.css('pointer-events', 'none');
                        elm.show();
                    } else {
                        $conteudo.css('-webkit-filter', 'blur(0px)');
                        $conteudo.css('cursor', 'default');
                        $conteudo.css('pointer-events', 'auto');
                        elm.hide();
                    }
                });
            }
        };

    }]);
"use strict";

var module = angular.module("module.directives");

module.directive("simplesPaginacao", function () {
	return {
		scope: {
			paginaDeRegistros: '=',
			pagina_onClick: '=paginaOnClick',
			pagina: '=',
			registrosPorPagina: '='
		},
		controller: function ($scope) {

			$scope.getTotalDePaginas = function (totalDeRegistros) {
				return Math.ceil(totalDeRegistros / $scope.registrosPorPagina);
			};

			$scope.getRangeDePaginas = function (totalDeRegistros) {
				var rangeDePaginas = new Array();
				var pagina = $scope.pagina;
				var paginaInicial = Math.max(1, pagina - 4);
				var paginaFinal = Math.max(1, Math.min($scope.getTotalDePaginas(totalDeRegistros), pagina + 4));
				while (paginaFinal - paginaInicial > 4) {
					if (paginaFinal - pagina >= pagina - paginaInicial) {
						paginaFinal--;
					} else {
						paginaInicial++;
					}
				}
				for (var i = paginaInicial; i <= paginaFinal; i++) {
					rangeDePaginas.push(i);
				}
				return rangeDePaginas;
			};

		},
		templateUrl: '/src/directives/paginacao.html'
	};
});

"use strict";

var module = angular.module('module.directives');

module.directive('passwordVerify', function () {
    return {
        restrict: 'A', // only activate on element attribute
        require: '?ngModel', // get a hold of NgModelController
        link: function (scope, elem, attrs, ngModel) {
            if (!ngModel) return; // do nothing if no ng-model

            // watch own value and re-validate on change
            scope.$watch(attrs.ngModel, function () {
                validate();
            });

            // observe the other value and re-validate on change
            attrs.$observe('passwordVerify', function (val) {
                validate();
            });

            var validate = function () {
                // values
                var val1 = ngModel.$viewValue;
                var val2 = attrs.passwordVerify;

                // set validity
                ngModel.$setValidity('passwordVerify', val1 === val2);
            };
        }
    }
});
var moduloSeguranca = angular.module('seguranca', []);

moduloSeguranca.directive('requerRole', ['$http', function ($http) {

    var roles = [];

    $http({
        url: '/api/usuario/roles',
        method: 'Get'
    }).then(function (result) {
        let data = result.data;
        if (!data) {
            window.location.href = 'login.html';
        }
        for (var i = 0; i < data.length; i++) {
            roles.push(data[i]);
        }
    });

    //pode receber uma role ou várias separadas por |
    function possuiRole(r, roles) {
        var rolesSplit = r.split('|');

        for (var i = 0; i < rolesSplit.length; i++) {

            for (var j = 0; j < roles.length; j++) {
                if (roles[j] == rolesSplit[i]) {//caso encontre o laço pode ser parado
                    return true;//efetua o break
                }
            }

        }

        return false;
    }

    return {
        link: function (scope, elm, attrs) {

            scope.roles = roles;

            scope.$watchCollection('roles', function (roles) {

                if (!possuiRole(attrs.requerRole, roles)) {
                    elm.hide();
                } else {
                    elm.show();
                }
            });

        }
    };

}]);
"use strict";

(function () {

    'use strict';

    var DATEPICKER_OPTS = {
        format: 'dd/mm/yyyy',
        language: 'pt-BR',
        weekStart: 0,
        disableTouchKeyboard: true,
        forceParse: false,
        todayHighlight: true,
        autoclose: true
    };

    var module = angular.module('module.directives');

    module.directive('simplesSeletorData', ['$filter', function ($filter) {
        return {
            require: 'ngModel',
            replace: true,
            link: function (scope, element, attrs, ngModelCtrl) {

                var $elm = $(element);

                scope.$watch(function () {
                    return ngModelCtrl.$modelValue;
                }, function () {
                    //$elm.datepicker('update');
                });

                var opts = angular.extend({}, DATEPICKER_OPTS, {
                    startDate: attrs['simplesDataInicial'],
                    endDate: attrs['simplesDataFinal']
                });

                function triggerHandler() {
                    angular.element(element).triggerHandler('input');
                }

                var mask = new StringMask('99/99/9999');
                $elm
                    .on('changeDate', triggerHandler)
                    .attr('maxlength', 10)
                    .datepicker(opts);
                mask.apply($elm)

                if (attrs['simplesReferenciaInicial']) {

                    $(attrs['simplesReferenciaInicial']).on('changeDate', function (e) {

                        $elm.datepicker('setStartDate', e.date);

                        var dataMinima = $(this).datepicker('getDate');
                        var dataAtual = $elm.datepicker('getDate');

                        if (dataAtual != null && dataMinima > dataAtual) {
                            $elm.datepicker('setDate', dataMinima);
                        }

                    });

                }

                if (attrs['simplesReferenciaFinal']) {

                    $(attrs['simplesReferenciaFinal']).on('changeDate', function (e) {

                        $elm.datepicker('setEndDate', e.date);

                        var dataMaxima = $(this).datepicker('getDate');
                        var dataAtual = $elm.datepicker('getDate');

                        if (dataAtual != null && dataMaxima < dataAtual) {
                            $elm.datepicker('setDate', dataMaxima);
                        }

                    });

                }

                ngModelCtrl.$formatters.unshift(function (value) {
                    return $filter('date')(value, 'dd/MM/yyyy');
                });

                ngModelCtrl.$parsers.push(function (value) {
                    return value === "" ? null : value;
                });

            }
        };
    }]);

})();
"use strict";

var module = angular.module('module.directives');

module.directive('somenteNumeros', function () {
    return {
        require: 'ngModel',
       
        link: function(scope, element, attrs, modelCtrl) {

        	var attr = attrs.somenteNumeros || '{}';
        	
        	var optionsDefault = { permitirNegativo: false };
        	var options = angular.extend(optionsDefault, eval("(" + attr + ")"));
        	
            modelCtrl.$parsers.push(function (inputValue) {
            	
            	var transformedInput = inputValue ? inputValue.replace(options.permitirNegativo ? /[^\d.-]/g : /[^\d]/g,'') : null;

                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});
"use strict";

angular.module("module.directives")

.directive('simplesTable', function () {
    return {
        restrict: 'A',
        scope: {
            simplesTableOptions: '='
        },

        controller: ['$scope', function ($scope) {

            var ths = $scope.ths = [];

            this.addTh = function (th) {               
                ths.push(th);
            };

            this.sort = function (campoSort, sortDir, element) {

                angular.forEach(ths, function (th) {
                    if (element != th) {
                        th.find('#icon-ordenacao').remove();
                    }
                });

                if (!$scope.simplesTableOptions) {
                    alert('Para efetuar uma ordena��o o atributo simplesTableOptions deve ser implementado na table');
                    return;
                }

                if (!$scope.simplesTableOptions.ordenarPor) {
                    alert('Para efetuar uma ordena��o a fun��o ordenarPor deve ser implementado no atributo simples-table-options');
                    return;
                }

                $scope.simplesTableOptions.ordenarPor({
                    campoSort: campoSort
                  , sortDir: sortDir
                });
            };
        }]

    };
})

.directive('simplesSort', function () {

    function link(scope, element, attrs, tableCtrl) {

        tableCtrl.addTh(element);

        element.addClass('simples-sort');
        
        element.on('click', function (event) {
            scope.sortAsc = !scope.sortAsc;

            var sortDir = scope.sortAsc ? 'asc' : 'desc'

            var icon = angular.element('<i id="icon-ordenacao" class="fa simples-table-sort-' + sortDir + '"></i>');

            element.find('#icon-ordenacao').remove();
            element.append(icon);

            tableCtrl.sort(attrs.simplesSort, sortDir, element);
        });
    };
    
    return {
        require: '^^simplesTable',
        restrict: 'A',
        link: link,
        scope: {}
    };
});
angular
    .module('module.directives')
    .directive('toupper', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (input) {
                    return input ? input.toUpperCase() : "";
                });
                element.css("text-transform", "uppercase");
            }
        };
    });
"use strict";

let mensagens = ['$timeout', '$interval', '$window', function ($timeout, $interval, $window) {

    let thisObj = {
        erros: [],
        sucesso: null,
        sucessoPreRoute: null
    };

    thisObj.mostraErros = function (mensagens, suffix) {
        let erros = [];
        for (let index in mensagens) {
            erros.push({texto: mensagens[index], surgimento: new Date().getTime()});
        }
        if (!suffix) {
            thisObj.erros = erros;
        } else {
            thisObj["erros" + suffix] = erros;
        }
        if (erros.length > 0) {
            thisObj.sucesso = null;
            $window.scrollTo(0, 0);
        }
    };

    thisObj.mostraSucesso = function (mensagem) {
        this.erros = [];
        thisObj.sucesso = mensagem;
        $window.scrollTo(0, 0);
    };

    thisObj.limparErros = function () {
        this.erros = [];
    };

    thisObj.limparSucesso = function () {
        this.sucesso = null;
    };

    thisObj.limpar = function () {
        this.sucesso = null;
        this.erros = [];
    };

    return thisObj;

}];

angular.module('module.helpers').factory('Mensagens', mensagens);

'use strict';

    angular
        .module('module.helpers')
        .factory('toastAlert', toastAlert);

    toastAlert.$inject = ['$mdToast'];

    function toastAlert($mdToast) {

        var service = {
            customToaster: customToaster,
            defaultToaster: defaultToaster
        };

        return service;

        function customToaster(content , delay, position) {
            $mdToast.show(
              $mdToast.simple()
                .textContent(content)
                .position(position)
                .hideDelay(delay)
            );
        }

        function defaultToaster(content) {
            $mdToast.show(
              $mdToast.simple()
                .textContent(content)
                .position('bottom right')
                .hideDelay(3000)
            );
        }
    }

'use strict';

var module = angular.module("module.helpers");

module.factory('ValidacaoHelper', ['$http', 'Mensagens', function ($http, Mensagens) {

    return {
        onError: function (rejection) {
            Mensagens.limpar();
            if (rejection.data.tipo_erro && rejection.data.tipo_erro == "erro_de_validacao") {
                Mensagens.mostraErros(['Existem campos obrigatórios não preenchidos']);
            } else if (rejection.data.tipo_erro && rejection.data.tipo_erro == "erro_de_negocio") {
                Mensagens.mostraErros([rejection.data.errors]);
            } else {
                //outros tipos de erro não tratado
                Mensagens.mostraErros(["Ocorreram problemas não tratados pelo sistema."]);
            }
        }
    };
}]);
'use strict';

angular.module('app').controller('AppController',
    ['$scope', '$state', '$timeout', '$stateParams', '$window', 'Mensagens', 'PilhaDeNavegacao', 'ValidacaoHelper', '$mdSidenav',
        function ($scope, $state, $timeout, $stateParams, $window, Mensagens, PilhaDeNavegacao, ValidacaoHelper, $mdSidenav) {

            $scope.abrirSideNav = function () {
                $mdSidenav('sidenav').toggle();
            };

            $scope.mensagens = Mensagens;

            $scope.btnFecharMensagemSucesso_onClick = function () {
                Mensagens.sucesso = null;
            };

            $scope.btnFecharMensagemErro_onClick = function () {
                Mensagens.limparErros();
            };

            $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

                Mensagens.mostraErros([]);

                Mensagens.sucesso = Mensagens.sucessoPreRoute;
                Mensagens.sucessoPreRoute = null;

                $window.scrollTo(0, 0);

                var problema = PilhaDeNavegacao.isTransicaoAutorizada(toState, toParams, fromState, fromParams);
                if (problema != null) {

                    event.preventDefault();

                    bootbox.alert(problema, function () {
                        PilhaDeNavegacao.reiniciar();
                    });
                }
            });
        }]);
'use strict';

angular.module('app').factory('CustomLoading', function () {

    var $conteudo = $('#conteudo');
    var $loading = $('.loading-spiner-holder');
    return {
        ativar: function () {
            $conteudo.css('cursor', 'wait');
            $conteudo.css('pointer-events', 'none');
            $loading.show();
        },

        desativar: function () {
            $conteudo.css('-webkit-filter', 'blur(0px)');
            $conteudo.css('cursor', 'default');
            $conteudo.css('pointer-events', 'auto');
            $loading.hide();
        }



    };

});
"use strict";

var module = angular.module("app");

module.service("PilhaDeNavegacao", [ '$state', '$stateParams', function ($state, $stateParams) {
	
	// armazena os metadados de todos os
	// produtores que suportam a pilha de
	// navegação. De maneira similar a configuração
	// de roteamento, os desenvolvedores devem
	// usar o método ".produtor(estado, internos)" para registrar
	// as funcionalidades capazes de trabalhar em
	// pilha. O parâmetro "internos" é um array opcional de 
	// estados que só são acessíveis pela funcionalidade 
	// produtora e que só navegam de volta para ela.
	var todosOsInternos = [];
	var produtores = {};
	
	// método de configuração que permite adicionar
	// produtores na PilhaDeNavegacao
	this.produtor = function (estado, internos) {
		if (produtores[estado] != null) {
			throw "Produtor já configurado! (" + estado + ")";
		}
		if ($.inArray(estado, todosOsInternos) >= 0) {
			throw "Um estado principal de produtor não pode ser interno de outro! (" + estado + ")";
		}
		for (var interno in internos) {
			if (produtores[interno] != null) {
				throw "Um estado principal de produtor não pode ser interno de outro! (" + interno + ")";	
			}
		}
		
		todosOsInternos = todosOsInternos.concat(internos);
		
		produtores[estado] = {
			internos: internos,
			transicoesPermitidas: internos.concat([estado])
		};
		
		return this;
	};

	//permite adicionar estados que não necessitam estar na pilha
	this.raiz = function(estado) {
	    raizes.push(estado);
	    return this;
	};
	
	// armazena um array de estados que podem ser
	// chamados a qualquer momento, cuja semântica é
	// reiniciar a pilha de navegação por completo quando
	// isso ocorrer.
	var raizes = [];
	
	// armazena o passo inicial da pilha
	// de navegação atual, se aplicável
	// cada passo possui a seguinte estrutura:
	// {
	//   origem: { estado, parametros },
	//   produtor: { estado, parametros, opcoes },
	//   $scope: escopo do controlador atual,
	//   anterior: null|passo
	// }
	var passo = null;
	
	// indica se estamos indo a um
	// novo passo de pilha ou retornando
	// de um passo atual, permitindo
	// que o gerenciador de transição bloqueie
	// navegações indesejadas ou mal programadas
	var concluindo = false;
	var empilhando = false;
	
	// armazena o passo concluído permitindo
	// que os controllers recebam as informações
	// de retorno do passo consumido
	var concluido = null;
	
	this.reiniciar = function () {
		
		passo = null;
		concluindo = false;
		empilhando = false;
		
		$state.go("inicio");
		
	};
	
	// este método é chamado pelo listener de troca
	// de estado da aplicação, e é responsável por
	// analisar a troca de view e autorizar ou não,
	// com base nas solicitações de empilhamento ou
	// conclusão que foram efetuadas pela view atual
	this.isTransicaoAutorizada = function (toState, toParams, fromState, fromParams) {
		
		if (concluindo) {
			
			// estamos concluindo um estado,
			// podemos permitir a operação
			
			concluindo = false;
			return null;
			
		} else {
			
			// higienização das variáveis de controle
			concluido = null;
			
		}
		
		if (empilhando) {
			empilhando = false;
			
			// estamos empilhando um estado,
			// portanto precisamos verificar se o
			// estado de destino é um produtor conhecido
			if (produtores[passo.produtor.estado] == null) {
				return "Produtor não configurado: '" + passo.produtor.estado + "'";
			}

			return null;
			
		}
		
		// estamos em uma transição desvinculada a pilha,
		// precisamos verificar se temos algum processo de
		// pilha em andamento
		if (passo == null) {
			
			// não temos um processo de pilha em andamento,
			// qualquer transição neste cenário é permitida
			return null;
			
		}
		
		// temos uma pilha, e uma transição desvinculada a ela,
		// que pode apenas ser permitida se for uma transição
		// para RAÍZ (caso em que resetaremos a pilha) ou para
		// views internas do passo atual
		
		// começaremos verificando se é uma transição para raíz
		if ($.inArray(toState.name, raizes) >= 0) {
			this.reiniciar();
			return null;
		}
		
		// depois verificamos se é uma transição interna, ou
		// uma transição para a própria tela principal do produtor
		var permitidasDoProdutor = produtores[passo.produtor.estado].transicoesPermitidas;
		if ($.inArray(toState.name, permitidasDoProdutor) >= 0) {
			return null;
		}
		
		// transição não autorizada
		return "Transição fora da pilha não autorizada! (de: " + fromState.name + "; para: " + toState.name + "; passo atual: " + passo.origem.estado + " -> " + passo.produtor.estado + ")";
	};
	
	// deve ser chamado sempre que uma tela
	// quiser iniciar ou adicionar um passo
	// em um empilhamento. O objeto produtor
	// tem a seguinte estrutura:
	// {
	//   estado, 
	//   parametros, 
	//   opcoes
	// }
	// o terceiro parâmetro, "descricaoDoPasso",
	// serve como um texto a ser apresentado
	// na migalha da funcionalidade produtora
	this.irPara = function (produtor, $scope, descricaoDoPasso) {
		
		// armazena como primeiro passo ou
		// encadeia o passo atual como anterior
		// a este, mantendo a stack
		var anterior = passo;
		
		passo = {
			origem: {
				estado: $state.current.name,
				parametros: angular.copy($stateParams)
			},
			produtor: produtor,
			$scope: $scope,
			anterior: anterior,
			descricao: descricaoDoPasso
		};
		
		empilhando = true;
		
		// se já não estamos na tela atual
		if (produtor.estado != $state.current.name) {
			// vamos para a nova tela
			$state.go(produtor.estado, produtor.parametros || {});
		} else {
			// recarregamos a tela atual
			$state.reload(produtor.estado, produtor.parametros || {});
		}
		
	};
	
	// deve ser chamado sempre que uma funcionalidade que
	// potencialmente está sendo chamada de forma empilhada
	// termina sua função. caso ela de fato tenha sido chamada
	// de forma empilhada, esse método redireciona o usuário
	// novamente para o passo anterior, repassando eventuais valores
	// de retorno para os respectivos callbacks, se aplicável
	this.retornar = function (valor) {
		
		// verifica se existe algum passo pendente
		// pois pode ser que essa funcionalidade tenha sido
		// chamada diretamente do menu ou diretamente pela URL
		if (passo == null) {
			return false;
		}
		else
		{
			
			// existe uma chamada pendente, indicando que essa
			// tela foi chamada de forma empilhada (através de
			// outra funcionalidade)
			concluindo = true;
			concluido = passo;
			concluido.valor = valor;
			passo = concluido.anterior;
			
			// se a tela anterior não é a mesma que a atual,
			// retornamos via $state.go, caso contrário,
			// simplesmente recarregamos com $state.reload
			if (concluido.origem.estado != $state.current.name) {
				$state.go(concluido.origem.estado, concluido.origem.parametros);
			} else {
				$state.reload(concluido.origem.estado, concluido.origem.parametros);
			}
			
			return true;
		}
		
	};
	
	// permite recuperar uma opção de configuração
	// do passo atual da pilha, ou retorna o valor
	// padrão se um passo deste não existir
	this.getOpcao = function (chave, padrao) {		
		return (passo != null && passo.produtor["opcoes"] != null) ? passo.produtor.opcoes[chave] : padrao;
	};
	
	// permite recuperar a descrição do passo
	// atualmente empilhado
	this.getDescricaoDoPasso = function () {
		return passo != null ? passo.descricao : "";
	};
	
	// este método deve ser chamado pelas telas que
	// consomem funcionalidade de pilha, de modo
	// a recuperarem o $scope anterior e processarem
	// o valor retornado pela tela consumida. uma
	// função de callback, recebendo o objeto
	// do produtor que concluiu e o valor retornado
	// pode ser passada, opcionalmente, para este método
	this.recuperar = function ($scope, callback) {
		if (concluido == null) {
			return false;
		}
		else
		{
			
			// transporta os dados necessários de um escopo
			// empilhado para um escopo novo. um tratamento
			// especial é feito para evitar o transporte de
			// propriedades nativas do angularjs ou funções
			for (var k in concluido.$scope) {
				if (typeof concluido.$scope[k] !== 'function' && !k.startsWith("$")) {
					$scope[k] = concluido.$scope[k];
				}
			}
			
			if (callback) {
				callback(concluido.produtor, concluido.valor);
			}
			
			return true;
		}
	};
	
}]);

function detalharImagemController($scope, $mdDialog) {
    var vm = this

    vm.$onInit = function () {
        vm.detalharObj = {
            imagem: {
                url: vm.objImagem.url,
                descricao: vm.objImagem.descricao
            },
            titulo: vm.titulo
        }
    };

    vm.classificacao = function (valor) {
        alert(valor)
        //vm.classificar()
    }

    vm.showAdvanced = function ($event) {
        var parentEl = angular.element(document.body);
        $mdDialog.show({
            parent: parentEl,
            targetEvent: $event,
            templateUrl: '/src/components/detalhar-imagem/modal/detalhar-imagem-modal.html',
            locals: {
                obj: vm.detalharObj,
                classificacao: vm.classificacao,
                lista: vm.lista
            },
            controller: DialogController
        });

        function DialogController($scope, $mdDialog, obj, classificacao, lista) {
            $scope.obj = obj;
            $scope.classificacao = classificacao;
            $scope.lista = lista;
            $scope.showCategorias = false;
            $scope.closeDialog = closeDialog;

            function closeDialog() {
                $mdDialog.hide();
            }
        }
    }
}

angular.module("detalhar-imagem.component", ['ngMaterial']).component('detalharImagemComponent', {
    bindings: {
        objImagem: '<',
        lista: '<',
        titulo: '@',
        classificar: '&'
    },
    templateUrl: '/src/components/detalhar-imagem/botao/detalhar-imagem.html',
    controller: detalharImagemController
})
function categorizarController($scope, $mdDialog) {
    var self = this;
    var pendingSearch, cancelSearch = angular.noop;
    var lastSearch;

    self.allCategorias = [];
    self.categorias = [];
    self.filterSelected = true;
    self.categorizar = categorizar;

    self.$onInit = function () {
        self.allCategorias = loadCategorias();
    };

    self.querySearch = querySearch;

    function querySearch(criteria) {
        return criteria ? self.allCategorias.filter(createFilterFor(criteria)) : [];
    }

    function refreshDebounce() {
        lastSearch = 0;
        pendingSearch = null;
        cancelSearch = angular.noop;
    }

    function categorizar() {
        var categorias = [];
        self.categorias.forEach(function (e){
            categorias.push(e.name);
        })
        alert(categorias);
    }

    function debounceSearch() {
        var now = new Date().getMilliseconds();
        lastSearch = lastSearch || now;

        return ((now - lastSearch) < 300);
    }

    function createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);

        return function filterFn(categoria) {
            return (categoria._lowername.indexOf(lowercaseQuery) != -1);
        };

    }

    function loadCategorias() {
        if (!self.lista)
            self.lista = [
                'Marina Augustine',
                'Oddr Sarno',
                'Nick Giannopoulos',
                'Jéssyca Mayara',
                'Anita Gros',
                'Megan Smith',
                'Ana Carolina Coretti',
                'Hector Simek',
                'Jedinho'
            ];

        return self.lista.map(function (c, index) {
            var categoria = {
                name: c
            };
            categoria._lowername = categoria.name.toLowerCase();
            return categoria;
        });
    }
}

angular.module("categorizar.component", ['ngMaterial']).component("categorizarComponent", {
    bindings: {
        lista: '<'
    },
    templateUrl: '/src/components/categorizar/categorizar.component.html',
    controller: categorizarController
})

angular.module('categorizar.module', [
    'categorizar.component'
])
angular.module("detalhar-imagem.module", [
    'detalhar-imagem.component',
    'categorizar.module'
])
angular.module("usuario-module", []);
'use strict';

angular.module('usuario-module').controller('UsuarioCadastrarController',
    ['$scope', '$state', '$stateParams', 'UsuarioService', 'ValidacaoHelper', 'Mensagens', '$mdDialog', 'idUsuario', 'exibePerfil',
        function ($scope, $state, $stateParams, UsuarioService, ValidacaoHelper, Mensagens, $mdDialog, idUsuario, exibePerfil) {

            $scope.id = idUsuario;
            $scope.exibePerfil = exibePerfil;

            _criarModel();
            _buscarUsuarioPorId();

            $scope.salvar = function () {
                Mensagens.limpar();
                if (!$scope.model.id) {
                    UsuarioService.salvar($scope.model).then(function (retorno) {
                        let usuario = retorno.data;
                        $mdDialog.hide(usuario);
                    }, ValidacaoHelper.onError);
                } else {
                    UsuarioService.alterar($scope.model.id, $scope.model).then(function (retorno) {
                        let usuario = retorno.data;
                        $mdDialog.hide(usuario);
                    }, ValidacaoHelper.onError);
                }
            };

            $scope.cancelar = function () {
                $mdDialog.cancel();
            };

            $scope.validarDuplicidadeDoLogin_blur = function () {
                _buscarUsuarioPorEmail();
            };

            function _buscarUsuarioPorId() {
                let id = $scope.id;
                if (id) {
                    UsuarioService.buscarUsuarioPorId(id).then((result) => {
                        let usuario = result.data;
                        if (usuario) {
                            $scope.model.id = usuario.id;
                            $scope.model.nome = usuario.nome;
                            $scope.model.email = usuario.email;
                            $scope.model.id_perfil = usuario.id_perfil;
                            $scope.model.senha = usuario.senha;
                        }
                    }, ValidacaoHelper.onError);
                }
            }

            function _buscarUsuarioPorEmail() {
                Mensagens.limpar();
                let email = $scope.model.email;
                if (email) {
                    UsuarioService.buscarUsuarioPorLogin(email).then(function (usuario) {
                        if (!usuario) {
                            Mensagens.mostraErros(['Usuário não encontrado']);
                        }
                    }, ValidacaoHelper.onError);
                }
            }

            function _criarModel() {
                $scope.formCadastro = {};
                $scope.model = {
                    id: null,
                    nome: null,
                    email: null,
                    id_perfil: 2,
                    senha: null,
                    repeticao_senha: null
                };
                _getPerfis();
            }

            function _getPerfis() {
                UsuarioService.buscarTodosPerfis().then((data) => {
                    if (data.status == 200)
                        $scope.perfis = data.data;
                }, ValidacaoHelper.onError);
            }

        }]);

'use strict';

angular.module('usuario-module').factory('UsuarioService',
    ['$http', function ($http) {

        return {
            buscarTodosPerfis: function () {
                return $http({
                    method: 'GET',
                    url: '/api/usuario/perfis'
                });
            },

            buscarTodosSetores: function () {
                return $http({
                    method: 'GET',
                    url: '/api/usuario/setores'
                });
            },

            buscarUsuarioPorLogin: function (login) {
                return $http({
                    method: 'GET',
                    url: '/api/usuario/por_login',
                    params: {login: login}
                });
            },

            buscarUsuarioPorId: function (id) {
                return $http({
                    method: 'GET',
                    url: '/api/usuario/' + id,
                });
            },

            salvar: function (usuario) {
                return $http({
                    method: 'POST',
                    url: '/api/usuario/salvar',
                    data: usuario
                });
            },

            alterar: function (id, usuario) {
                return $http({
                    method: 'PUT',
                    url: '/api/usuario/alterar/' + id,
                    data: usuario
                });
            },

            consultar: function (filtro) {
                return $http({
                    method: 'GET',
                    url: '/api/usuario',
                    params: filtro
                });
            },

            excluir: function (id) {
                return $http({
                    method: 'DELETE',
                    url: '/api/usuario/' + id
                });
            },

            resetarSenha: function (id) {
                return $http({
                    method: 'PUT',
                    url: '/api/usuario/resetar-senha/' + id
                });
            }
        };
    }]);
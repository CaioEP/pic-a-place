'use strict';

angular.module('login').controller('LoginController',
    ['$scope', '$state', 'LoginService', 'Mensagens', 'ValidacaoHelper', '$mdDialog',
        function ($scope, $state, LoginService, Mensagens, ValidacaoHelper, $mdDialog) {

            /* INICIO DO FORM DE LOGIN */
            _criarModel();

            $scope.entrar = function () {
                Mensagens.limpar();
                LoginService.entrar($scope.model).then(function () {
                    Mensagens.mostraSucesso("Login efetuado com sucesso");
                    setTimeout(function () {
                        window.location.href = '/';
                    }, 1500);
                }, ValidacaoHelper.onError);
            };

            $scope.registrar = function (ev) {
                Mensagens.limpar();
                $mdDialog.show({
                    controller: 'UsuarioCadastrarController',
                    templateUrl: '/src/compartilhado/usuario/usuario.cadastrar.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: true,
                    locals: {
                        idUsuario: null,
                        exibePerfil: false
                    }
                }).then(function (usuario) {
                    if (usuario) {
                        $scope.model = usuario;
                        $scope.entrar();
                    }
                });
            };

            function _criarModel() {
                $scope.model = {
                    email: null,
                    senha: null
                };
            }
            /* FIM DO FORM DE LOGIN */




            $scope.mensagens = Mensagens;

            $scope.btnFecharMensagemSucesso_onClick = function () {
                Mensagens.sucesso = null;
            };

            $scope.btnFecharMensagemErro_onClick = function () {
                Mensagens.limparErros();
            };

            $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

                Mensagens.mostraErros([]);

                Mensagens.sucesso = Mensagens.sucessoPreRoute;
                Mensagens.sucessoPreRoute = null;

                $window.scrollTo(0, 0);

                let problema = PilhaDeNavegacao.isTransicaoAutorizada(toState, toParams, fromState, fromParams);
                if (problema != null) {

                    event.preventDefault();

                    bootbox.alert(problema, function () {
                        PilhaDeNavegacao.reiniciar();
                    });
                }
            });

        }]);
'use strict';

angular.module('login').config([ '$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('login', {
    		url: '/',
    		templateUrl: '/login.html',
    		controller: 'LoginController'
    	});
}]);


'use strict';

angular.module('login').factory('LoginService', ['$http',
    function ($http) {

        return {
            entrar: function (registro) {
                return $http({
                    method: 'POST',
                    url: '/api/login',
                    data: registro
                });
            }

        };
    }]);
"use strict";

var module = angular.module("module.directives");

module.directive('completaComZero', function () {

    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {

            element.bind('blur', function () {
            	
            	var attr = attrs.completaComZero || '{}';
            	var optionsDefault = { preencherQuandoVazio: true };
            	var options = angular.extend(optionsDefault, eval("(" + attr + ")"));

                var numero = parseInt(modelCtrl.$viewValue, 10);
                var tamanho = parseInt(attrs.completaComZero, 10);
                
                var preencher = function(numero, tamanho){
                	numero = '' + numero;
                    while (numero.length < tamanho) {
                        numero = '0' + numero;
                    }
                    modelCtrl.$setViewValue(numero);
                    modelCtrl.$render();
                }
                
                if (isNaN(numero) || isNaN(tamanho)) {
                	 if(options.preencherQuandoVazio == true){
                		 preencher(0, tamanho);
                	 }
                }else{
                	preencher(numero, tamanho);
                }
            });
        }
    }
});
angular.module("module.directives").directive('loading',
    ['$http', function ($http) {
        return {
            restrict: 'A',
            link: function (scope, elm, attrs) {
                scope.isLoading = function () {
                    var requests = $http.pendingRequests;
                    var retorno = false;
                    requests.forEach(function (request) {
                        retorno = !request.desabilitarLoading;
                    });
                    return retorno && $http.pendingRequests.length > 0;
                };

                scope.$watch(scope.isLoading, function (v) {
                    var $conteudo = $('#conteudo');
                    if (v) {
                        $conteudo.css('-webkit-filter', 'blur(2px)');
                        $conteudo.css('cursor', 'wait');
                        $conteudo.css('pointer-events', 'none');
                        elm.show();
                    } else {
                        $conteudo.css('-webkit-filter', 'blur(0px)');
                        $conteudo.css('cursor', 'default');
                        $conteudo.css('pointer-events', 'auto');
                        elm.hide();
                    }
                });
            }
        };

    }]);
"use strict";

var module = angular.module("module.directives");

module.directive("simplesPaginacao", function () {
	return {
		scope: {
			paginaDeRegistros: '=',
			pagina_onClick: '=paginaOnClick',
			pagina: '=',
			registrosPorPagina: '='
		},
		controller: function ($scope) {

			$scope.getTotalDePaginas = function (totalDeRegistros) {
				return Math.ceil(totalDeRegistros / $scope.registrosPorPagina);
			};

			$scope.getRangeDePaginas = function (totalDeRegistros) {
				var rangeDePaginas = new Array();
				var pagina = $scope.pagina;
				var paginaInicial = Math.max(1, pagina - 4);
				var paginaFinal = Math.max(1, Math.min($scope.getTotalDePaginas(totalDeRegistros), pagina + 4));
				while (paginaFinal - paginaInicial > 4) {
					if (paginaFinal - pagina >= pagina - paginaInicial) {
						paginaFinal--;
					} else {
						paginaInicial++;
					}
				}
				for (var i = paginaInicial; i <= paginaFinal; i++) {
					rangeDePaginas.push(i);
				}
				return rangeDePaginas;
			};

		},
		templateUrl: '/src/directives/paginacao.html'
	};
});

"use strict";

var module = angular.module('module.directives');

module.directive('passwordVerify', function () {
    return {
        restrict: 'A', // only activate on element attribute
        require: '?ngModel', // get a hold of NgModelController
        link: function (scope, elem, attrs, ngModel) {
            if (!ngModel) return; // do nothing if no ng-model

            // watch own value and re-validate on change
            scope.$watch(attrs.ngModel, function () {
                validate();
            });

            // observe the other value and re-validate on change
            attrs.$observe('passwordVerify', function (val) {
                validate();
            });

            var validate = function () {
                // values
                var val1 = ngModel.$viewValue;
                var val2 = attrs.passwordVerify;

                // set validity
                ngModel.$setValidity('passwordVerify', val1 === val2);
            };
        }
    }
});
var moduloSeguranca = angular.module('seguranca', []);

moduloSeguranca.directive('requerRole', ['$http', function ($http) {

    var roles = [];

    $http({
        url: '/api/usuario/roles',
        method: 'Get'
    }).then(function (result) {
        let data = result.data;
        if (!data) {
            window.location.href = 'login.html';
        }
        for (var i = 0; i < data.length; i++) {
            roles.push(data[i]);
        }
    });

    //pode receber uma role ou várias separadas por |
    function possuiRole(r, roles) {
        var rolesSplit = r.split('|');

        for (var i = 0; i < rolesSplit.length; i++) {

            for (var j = 0; j < roles.length; j++) {
                if (roles[j] == rolesSplit[i]) {//caso encontre o laço pode ser parado
                    return true;//efetua o break
                }
            }

        }

        return false;
    }

    return {
        link: function (scope, elm, attrs) {

            scope.roles = roles;

            scope.$watchCollection('roles', function (roles) {

                if (!possuiRole(attrs.requerRole, roles)) {
                    elm.hide();
                } else {
                    elm.show();
                }
            });

        }
    };

}]);
"use strict";

(function () {

    'use strict';

    var DATEPICKER_OPTS = {
        format: 'dd/mm/yyyy',
        language: 'pt-BR',
        weekStart: 0,
        disableTouchKeyboard: true,
        forceParse: false,
        todayHighlight: true,
        autoclose: true
    };

    var module = angular.module('module.directives');

    module.directive('simplesSeletorData', ['$filter', function ($filter) {
        return {
            require: 'ngModel',
            replace: true,
            link: function (scope, element, attrs, ngModelCtrl) {

                var $elm = $(element);

                scope.$watch(function () {
                    return ngModelCtrl.$modelValue;
                }, function () {
                    //$elm.datepicker('update');
                });

                var opts = angular.extend({}, DATEPICKER_OPTS, {
                    startDate: attrs['simplesDataInicial'],
                    endDate: attrs['simplesDataFinal']
                });

                function triggerHandler() {
                    angular.element(element).triggerHandler('input');
                }

                var mask = new StringMask('99/99/9999');
                $elm
                    .on('changeDate', triggerHandler)
                    .attr('maxlength', 10)
                    .datepicker(opts);
                mask.apply($elm)

                if (attrs['simplesReferenciaInicial']) {

                    $(attrs['simplesReferenciaInicial']).on('changeDate', function (e) {

                        $elm.datepicker('setStartDate', e.date);

                        var dataMinima = $(this).datepicker('getDate');
                        var dataAtual = $elm.datepicker('getDate');

                        if (dataAtual != null && dataMinima > dataAtual) {
                            $elm.datepicker('setDate', dataMinima);
                        }

                    });

                }

                if (attrs['simplesReferenciaFinal']) {

                    $(attrs['simplesReferenciaFinal']).on('changeDate', function (e) {

                        $elm.datepicker('setEndDate', e.date);

                        var dataMaxima = $(this).datepicker('getDate');
                        var dataAtual = $elm.datepicker('getDate');

                        if (dataAtual != null && dataMaxima < dataAtual) {
                            $elm.datepicker('setDate', dataMaxima);
                        }

                    });

                }

                ngModelCtrl.$formatters.unshift(function (value) {
                    return $filter('date')(value, 'dd/MM/yyyy');
                });

                ngModelCtrl.$parsers.push(function (value) {
                    return value === "" ? null : value;
                });

            }
        };
    }]);

})();
"use strict";

var module = angular.module('module.directives');

module.directive('somenteNumeros', function () {
    return {
        require: 'ngModel',
       
        link: function(scope, element, attrs, modelCtrl) {

        	var attr = attrs.somenteNumeros || '{}';
        	
        	var optionsDefault = { permitirNegativo: false };
        	var options = angular.extend(optionsDefault, eval("(" + attr + ")"));
        	
            modelCtrl.$parsers.push(function (inputValue) {
            	
            	var transformedInput = inputValue ? inputValue.replace(options.permitirNegativo ? /[^\d.-]/g : /[^\d]/g,'') : null;

                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});
"use strict";

angular.module("module.directives")

.directive('simplesTable', function () {
    return {
        restrict: 'A',
        scope: {
            simplesTableOptions: '='
        },

        controller: ['$scope', function ($scope) {

            var ths = $scope.ths = [];

            this.addTh = function (th) {               
                ths.push(th);
            };

            this.sort = function (campoSort, sortDir, element) {

                angular.forEach(ths, function (th) {
                    if (element != th) {
                        th.find('#icon-ordenacao').remove();
                    }
                });

                if (!$scope.simplesTableOptions) {
                    alert('Para efetuar uma ordena��o o atributo simplesTableOptions deve ser implementado na table');
                    return;
                }

                if (!$scope.simplesTableOptions.ordenarPor) {
                    alert('Para efetuar uma ordena��o a fun��o ordenarPor deve ser implementado no atributo simples-table-options');
                    return;
                }

                $scope.simplesTableOptions.ordenarPor({
                    campoSort: campoSort
                  , sortDir: sortDir
                });
            };
        }]

    };
})

.directive('simplesSort', function () {

    function link(scope, element, attrs, tableCtrl) {

        tableCtrl.addTh(element);

        element.addClass('simples-sort');
        
        element.on('click', function (event) {
            scope.sortAsc = !scope.sortAsc;

            var sortDir = scope.sortAsc ? 'asc' : 'desc'

            var icon = angular.element('<i id="icon-ordenacao" class="fa simples-table-sort-' + sortDir + '"></i>');

            element.find('#icon-ordenacao').remove();
            element.append(icon);

            tableCtrl.sort(attrs.simplesSort, sortDir, element);
        });
    };
    
    return {
        require: '^^simplesTable',
        restrict: 'A',
        link: link,
        scope: {}
    };
});
angular
    .module('module.directives')
    .directive('toupper', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (input) {
                    return input ? input.toUpperCase() : "";
                });
                element.css("text-transform", "uppercase");
            }
        };
    });
"use strict";

let mensagens = ['$timeout', '$interval', '$window', function ($timeout, $interval, $window) {

    let thisObj = {
        erros: [],
        sucesso: null,
        sucessoPreRoute: null
    };

    thisObj.mostraErros = function (mensagens, suffix) {
        let erros = [];
        for (let index in mensagens) {
            erros.push({texto: mensagens[index], surgimento: new Date().getTime()});
        }
        if (!suffix) {
            thisObj.erros = erros;
        } else {
            thisObj["erros" + suffix] = erros;
        }
        if (erros.length > 0) {
            thisObj.sucesso = null;
            $window.scrollTo(0, 0);
        }
    };

    thisObj.mostraSucesso = function (mensagem) {
        this.erros = [];
        thisObj.sucesso = mensagem;
        $window.scrollTo(0, 0);
    };

    thisObj.limparErros = function () {
        this.erros = [];
    };

    thisObj.limparSucesso = function () {
        this.sucesso = null;
    };

    thisObj.limpar = function () {
        this.sucesso = null;
        this.erros = [];
    };

    return thisObj;

}];

angular.module('module.helpers').factory('Mensagens', mensagens);

'use strict';

var module = angular.module("module.helpers");

module.factory('ValidacaoHelper', ['$http', 'Mensagens', function ($http, Mensagens) {

    return {
        onError: function (rejection) {
            Mensagens.limpar();
            if (rejection.data.tipo_erro && rejection.data.tipo_erro == "erro_de_validacao") {
                Mensagens.mostraErros(['Existem campos obrigatórios não preenchidos']);
            } else if (rejection.data.tipo_erro && rejection.data.tipo_erro == "erro_de_negocio") {
                Mensagens.mostraErros([rejection.data.errors]);
            } else {
                //outros tipos de erro não tratado
                Mensagens.mostraErros(["Ocorreram problemas não tratados pelo sistema."]);
            }
        }
    };
}]);
angular.module("usuario-module", []);
'use strict';

angular.module('usuario-module').controller('UsuarioCadastrarController',
    ['$scope', '$state', '$stateParams', 'UsuarioService', 'ValidacaoHelper', 'Mensagens', '$mdDialog', 'idUsuario', 'exibePerfil',
        function ($scope, $state, $stateParams, UsuarioService, ValidacaoHelper, Mensagens, $mdDialog, idUsuario, exibePerfil) {

            $scope.id = idUsuario;
            $scope.exibePerfil = exibePerfil;

            _criarModel();
            _buscarUsuarioPorId();

            $scope.salvar = function () {
                Mensagens.limpar();
                if (!$scope.model.id) {
                    UsuarioService.salvar($scope.model).then(function (retorno) {
                        let usuario = retorno.data;
                        $mdDialog.hide(usuario);
                    }, ValidacaoHelper.onError);
                } else {
                    UsuarioService.alterar($scope.model.id, $scope.model).then(function (retorno) {
                        let usuario = retorno.data;
                        $mdDialog.hide(usuario);
                    }, ValidacaoHelper.onError);
                }
            };

            $scope.cancelar = function () {
                $mdDialog.cancel();
            };

            $scope.validarDuplicidadeDoLogin_blur = function () {
                _buscarUsuarioPorEmail();
            };

            function _buscarUsuarioPorId() {
                let id = $scope.id;
                if (id) {
                    UsuarioService.buscarUsuarioPorId(id).then((result) => {
                        let usuario = result.data;
                        if (usuario) {
                            $scope.model.id = usuario.id;
                            $scope.model.nome = usuario.nome;
                            $scope.model.email = usuario.email;
                            $scope.model.id_perfil = usuario.id_perfil;
                            $scope.model.senha = usuario.senha;
                        }
                    }, ValidacaoHelper.onError);
                }
            }

            function _buscarUsuarioPorEmail() {
                Mensagens.limpar();
                let email = $scope.model.email;
                if (email) {
                    UsuarioService.buscarUsuarioPorLogin(email).then(function (usuario) {
                        if (!usuario) {
                            Mensagens.mostraErros(['Usuário não encontrado']);
                        }
                    }, ValidacaoHelper.onError);
                }
            }

            function _criarModel() {
                $scope.formCadastro = {};
                $scope.model = {
                    id: null,
                    nome: null,
                    email: null,
                    id_perfil: 2,
                    senha: null,
                    repeticao_senha: null
                };
                _getPerfis();
            }

            function _getPerfis() {
                UsuarioService.buscarTodosPerfis().then((data) => {
                    if (data.status == 200)
                        $scope.perfis = data.data;
                }, ValidacaoHelper.onError);
            }

        }]);

'use strict';

angular.module('usuario-module').factory('UsuarioService',
    ['$http', function ($http) {

        return {
            buscarTodosPerfis: function () {
                return $http({
                    method: 'GET',
                    url: '/api/usuario/perfis'
                });
            },

            buscarTodosSetores: function () {
                return $http({
                    method: 'GET',
                    url: '/api/usuario/setores'
                });
            },

            buscarUsuarioPorLogin: function (login) {
                return $http({
                    method: 'GET',
                    url: '/api/usuario/por_login',
                    params: {login: login}
                });
            },

            buscarUsuarioPorId: function (id) {
                return $http({
                    method: 'GET',
                    url: '/api/usuario/' + id,
                });
            },

            salvar: function (usuario) {
                return $http({
                    method: 'POST',
                    url: '/api/usuario/salvar',
                    data: usuario
                });
            },

            alterar: function (id, usuario) {
                return $http({
                    method: 'PUT',
                    url: '/api/usuario/alterar/' + id,
                    data: usuario
                });
            },

            consultar: function (filtro) {
                return $http({
                    method: 'GET',
                    url: '/api/usuario',
                    params: filtro
                });
            },

            excluir: function (id) {
                return $http({
                    method: 'DELETE',
                    url: '/api/usuario/' + id
                });
            },

            resetarSenha: function (id) {
                return $http({
                    method: 'PUT',
                    url: '/api/usuario/resetar-senha/' + id
                });
            }
        };
    }]);
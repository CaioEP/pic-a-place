'use strict';

angular.module('usuario-module').factory('UsuarioService',
    ['$http', function ($http) {

        return {
            buscarTodosPerfis: function () {
                return $http({
                    method: 'GET',
                    url: '/api/usuario/perfis'
                });
            },

            buscarTodosSetores: function () {
                return $http({
                    method: 'GET',
                    url: '/api/usuario/setores'
                });
            },

            buscarUsuarioPorLogin: function (login) {
                return $http({
                    method: 'GET',
                    url: '/api/usuario/por_login',
                    params: {login: login}
                });
            },

            buscarUsuarioPorId: function (id) {
                return $http({
                    method: 'GET',
                    url: '/api/usuario/' + id,
                });
            },

            salvar: function (usuario) {
                return $http({
                    method: 'POST',
                    url: '/api/usuario/salvar',
                    data: usuario
                });
            },

            alterar: function (id, usuario) {
                return $http({
                    method: 'PUT',
                    url: '/api/usuario/alterar/' + id,
                    data: usuario
                });
            },

            consultar: function (filtro) {
                return $http({
                    method: 'GET',
                    url: '/api/usuario',
                    params: filtro
                });
            },

            excluir: function (id) {
                return $http({
                    method: 'DELETE',
                    url: '/api/usuario/' + id
                });
            },

            resetarSenha: function (id) {
                return $http({
                    method: 'PUT',
                    url: '/api/usuario/resetar-senha/' + id
                });
            }
        };
    }]);
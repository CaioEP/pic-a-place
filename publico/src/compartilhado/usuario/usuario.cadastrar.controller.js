'use strict';

angular.module('usuario-module').controller('UsuarioCadastrarController',
    ['$scope', '$state', '$stateParams', 'UsuarioService', 'ValidacaoHelper', 'Mensagens', '$mdDialog', 'idUsuario', 'exibePerfil',
        function ($scope, $state, $stateParams, UsuarioService, ValidacaoHelper, Mensagens, $mdDialog, idUsuario, exibePerfil) {

            $scope.id = idUsuario;
            $scope.exibePerfil = exibePerfil;

            _criarModel();
            _buscarUsuarioPorId();

            $scope.salvar = function () {
                Mensagens.limpar();
                if (!$scope.model.id) {
                    UsuarioService.salvar($scope.model).then(function (retorno) {
                        let usuario = retorno.data;
                        $mdDialog.hide(usuario);
                    }, ValidacaoHelper.onError);
                } else {
                    UsuarioService.alterar($scope.model.id, $scope.model).then(function (retorno) {
                        let usuario = retorno.data;
                        $mdDialog.hide(usuario);
                    }, ValidacaoHelper.onError);
                }
            };

            $scope.cancelar = function () {
                $mdDialog.cancel();
            };

            $scope.validarDuplicidadeDoLogin_blur = function () {
                _buscarUsuarioPorEmail();
            };

            function _buscarUsuarioPorId() {
                let id = $scope.id;
                if (id) {
                    UsuarioService.buscarUsuarioPorId(id).then((result) => {
                        let usuario = result.data;
                        if (usuario) {
                            $scope.model.id = usuario.id;
                            $scope.model.nome = usuario.nome;
                            $scope.model.email = usuario.email;
                            $scope.model.id_perfil = usuario.id_perfil;
                            $scope.model.senha = usuario.senha;
                        }
                    }, ValidacaoHelper.onError);
                }
            }

            function _buscarUsuarioPorEmail() {
                Mensagens.limpar();
                let email = $scope.model.email;
                if (email) {
                    UsuarioService.buscarUsuarioPorLogin(email).then(function (usuario) {
                        if (!usuario) {
                            Mensagens.mostraErros(['Usuário não encontrado']);
                        }
                    }, ValidacaoHelper.onError);
                }
            }

            function _criarModel() {
                $scope.formCadastro = {};
                $scope.model = {
                    id: null,
                    nome: null,
                    email: null,
                    id_perfil: 2,
                    senha: null,
                    repeticao_senha: null
                };
                _getPerfis();
            }

            function _getPerfis() {
                UsuarioService.buscarTodosPerfis().then((data) => {
                    if (data.status == 200)
                        $scope.perfis = data.data;
                }, ValidacaoHelper.onError);
            }

        }]);

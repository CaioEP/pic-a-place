'use strict';

angular.module('login').factory('LoginService', ['$http',
    function ($http) {

        return {
            entrar: function (registro) {
                return $http({
                    method: 'POST',
                    url: '/api/login',
                    data: registro
                });
            }

        };
    }]);
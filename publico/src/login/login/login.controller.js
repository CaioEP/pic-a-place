'use strict';

angular.module('login').controller('LoginController',
    ['$scope', '$state', 'LoginService', 'Mensagens', 'ValidacaoHelper', '$mdDialog',
        function ($scope, $state, LoginService, Mensagens, ValidacaoHelper, $mdDialog) {

            /* INICIO DO FORM DE LOGIN */
            _criarModel();

            $scope.entrar = function () {
                Mensagens.limpar();
                LoginService.entrar($scope.model).then(function () {
                    Mensagens.mostraSucesso("Login efetuado com sucesso");
                    setTimeout(function () {
                        window.location.href = '/';
                    }, 1500);
                }, ValidacaoHelper.onError);
            };

            $scope.registrar = function (ev) {
                Mensagens.limpar();
                $mdDialog.show({
                    controller: 'UsuarioCadastrarController',
                    templateUrl: '/src/compartilhado/usuario/usuario.cadastrar.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: true,
                    locals: {
                        idUsuario: null,
                        exibePerfil: false
                    }
                }).then(function (usuario) {
                    if (usuario) {
                        $scope.model = usuario;
                        $scope.entrar();
                    }
                });
            };

            function _criarModel() {
                $scope.model = {
                    email: null,
                    senha: null
                };
            }
            /* FIM DO FORM DE LOGIN */




            $scope.mensagens = Mensagens;

            $scope.btnFecharMensagemSucesso_onClick = function () {
                Mensagens.sucesso = null;
            };

            $scope.btnFecharMensagemErro_onClick = function () {
                Mensagens.limparErros();
            };

            $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

                Mensagens.mostraErros([]);

                Mensagens.sucesso = Mensagens.sucessoPreRoute;
                Mensagens.sucessoPreRoute = null;

                $window.scrollTo(0, 0);

                let problema = PilhaDeNavegacao.isTransicaoAutorizada(toState, toParams, fromState, fromParams);
                if (problema != null) {

                    event.preventDefault();

                    bootbox.alert(problema, function () {
                        PilhaDeNavegacao.reiniciar();
                    });
                }
            });

        }]);
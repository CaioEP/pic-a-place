'use strict';

angular.module('login').config([ '$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('login', {
    		url: '/',
    		templateUrl: '/login.html',
    		controller: 'LoginController'
    	});
}]);


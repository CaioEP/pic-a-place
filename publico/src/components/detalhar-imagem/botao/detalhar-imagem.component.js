function detalharImagemController($scope, $mdDialog) {
    var vm = this

    vm.$onInit = function () {
        vm.detalharObj = {
            imagem: {
                url: vm.objImagem.url,
                descricao: vm.objImagem.descricao
            },
            titulo: vm.titulo
        }
    };

    vm.classificacao = function (valor) {
        alert(valor)
        //vm.classificar()
    }

    vm.showAdvanced = function ($event) {
        var parentEl = angular.element(document.body);
        $mdDialog.show({
            parent: parentEl,
            targetEvent: $event,
            templateUrl: '/src/components/detalhar-imagem/modal/detalhar-imagem-modal.html',
            locals: {
                obj: vm.detalharObj,
                classificacao: vm.classificacao,
                lista: vm.lista
            },
            controller: DialogController
        });

        function DialogController($scope, $mdDialog, obj, classificacao, lista) {
            $scope.obj = obj;
            $scope.classificacao = classificacao;
            $scope.lista = lista;
            $scope.showCategorias = false;
            $scope.closeDialog = closeDialog;

            function closeDialog() {
                $mdDialog.hide();
            }
        }
    }
}

angular.module("detalhar-imagem.component", ['ngMaterial']).component('detalharImagemComponent', {
    bindings: {
        objImagem: '<',
        lista: '<',
        titulo: '@',
        classificar: '&'
    },
    templateUrl: '/src/components/detalhar-imagem/botao/detalhar-imagem.html',
    controller: detalharImagemController
})
function categorizarController($scope, $mdDialog) {
    var self = this;
    var pendingSearch, cancelSearch = angular.noop;
    var lastSearch;

    self.allCategorias = [];
    self.categorias = [];
    self.filterSelected = true;
    self.categorizar = categorizar;

    self.$onInit = function () {
        self.allCategorias = loadCategorias();
    };

    self.querySearch = querySearch;

    function querySearch(criteria) {
        return criteria ? self.allCategorias.filter(createFilterFor(criteria)) : [];
    }

    function refreshDebounce() {
        lastSearch = 0;
        pendingSearch = null;
        cancelSearch = angular.noop;
    }

    function categorizar() {
        var categorias = [];
        self.categorias.forEach(function (e){
            categorias.push(e.name);
        })
        alert(categorias);
    }

    function debounceSearch() {
        var now = new Date().getMilliseconds();
        lastSearch = lastSearch || now;

        return ((now - lastSearch) < 300);
    }

    function createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);

        return function filterFn(categoria) {
            return (categoria._lowername.indexOf(lowercaseQuery) != -1);
        };

    }

    function loadCategorias() {
        if (!self.lista)
            self.lista = [
                'Marina Augustine',
                'Oddr Sarno',
                'Nick Giannopoulos',
                'Jéssyca Mayara',
                'Anita Gros',
                'Megan Smith',
                'Ana Carolina Coretti',
                'Hector Simek',
                'Jedinho'
            ];

        return self.lista.map(function (c, index) {
            var categoria = {
                name: c
            };
            categoria._lowername = categoria.name.toLowerCase();
            return categoria;
        });
    }
}

angular.module("categorizar.component", ['ngMaterial']).component("categorizarComponent", {
    bindings: {
        lista: '<'
    },
    templateUrl: '/src/components/categorizar/categorizar.component.html',
    controller: categorizarController
})

'use strict';

angular.module('app').controller('AppController',
    ['$scope', '$state', '$timeout', '$stateParams', '$window', 'Mensagens', 'PilhaDeNavegacao', 'ValidacaoHelper', '$mdSidenav', 'Inicio', '$mdDialog',
        function ($scope, $state, $timeout, $stateParams, $window, Mensagens, PilhaDeNavegacao, ValidacaoHelper, $mdSidenav, Inicio, $mdDialog) {

            $scope.abrirSideNav = function () {
                $mdSidenav('sidenav').toggle();
            };

            $scope.sair = function ($event) {
                let confirm = $mdDialog.confirm()
                    .title('Deseja sair do sistema?')
                    .textContent('Será redirecionado para a tela de Login')
                    .targetEvent($event)
                    .ok('Sim')
                    .cancel('Não');

                $mdDialog.show(confirm).then(function () {
                    Inicio.sair().then(result => {
                        if (result.status = 200) {
                            window.location.href = '/login.html';
                        }
                    }, ValidacaoHelper.onError);
                });

            };

            $scope.mensagens = Mensagens;

            $scope.btnFecharMensagemSucesso_onClick = function () {
                Mensagens.sucesso = null;
            };

            $scope.btnFecharMensagemErro_onClick = function () {
                Mensagens.limparErros();
            };

            $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

                Mensagens.mostraErros([]);

                Mensagens.sucesso = Mensagens.sucessoPreRoute;
                Mensagens.sucessoPreRoute = null;

                $window.scrollTo(0, 0);

                var problema = PilhaDeNavegacao.isTransicaoAutorizada(toState, toParams, fromState, fromParams);
                if (problema != null) {

                    event.preventDefault();

                    bootbox.alert(problema, function () {
                        PilhaDeNavegacao.reiniciar();
                    });
                }
            });
        }]);
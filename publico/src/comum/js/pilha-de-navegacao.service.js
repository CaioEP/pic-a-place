"use strict";

var module = angular.module("app");

module.service("PilhaDeNavegacao", [ '$state', '$stateParams', function ($state, $stateParams) {
	
	// armazena os metadados de todos os
	// produtores que suportam a pilha de
	// navegação. De maneira similar a configuração
	// de roteamento, os desenvolvedores devem
	// usar o método ".produtor(estado, internos)" para registrar
	// as funcionalidades capazes de trabalhar em
	// pilha. O parâmetro "internos" é um array opcional de 
	// estados que só são acessíveis pela funcionalidade 
	// produtora e que só navegam de volta para ela.
	var todosOsInternos = [];
	var produtores = {};
	
	// método de configuração que permite adicionar
	// produtores na PilhaDeNavegacao
	this.produtor = function (estado, internos) {
		if (produtores[estado] != null) {
			throw "Produtor já configurado! (" + estado + ")";
		}
		if ($.inArray(estado, todosOsInternos) >= 0) {
			throw "Um estado principal de produtor não pode ser interno de outro! (" + estado + ")";
		}
		for (var interno in internos) {
			if (produtores[interno] != null) {
				throw "Um estado principal de produtor não pode ser interno de outro! (" + interno + ")";	
			}
		}
		
		todosOsInternos = todosOsInternos.concat(internos);
		
		produtores[estado] = {
			internos: internos,
			transicoesPermitidas: internos.concat([estado])
		};
		
		return this;
	};

	//permite adicionar estados que não necessitam estar na pilha
	this.raiz = function(estado) {
	    raizes.push(estado);
	    return this;
	};
	
	// armazena um array de estados que podem ser
	// chamados a qualquer momento, cuja semântica é
	// reiniciar a pilha de navegação por completo quando
	// isso ocorrer.
	var raizes = [];
	
	// armazena o passo inicial da pilha
	// de navegação atual, se aplicável
	// cada passo possui a seguinte estrutura:
	// {
	//   origem: { estado, parametros },
	//   produtor: { estado, parametros, opcoes },
	//   $scope: escopo do controlador atual,
	//   anterior: null|passo
	// }
	var passo = null;
	
	// indica se estamos indo a um
	// novo passo de pilha ou retornando
	// de um passo atual, permitindo
	// que o gerenciador de transição bloqueie
	// navegações indesejadas ou mal programadas
	var concluindo = false;
	var empilhando = false;
	
	// armazena o passo concluído permitindo
	// que os controllers recebam as informações
	// de retorno do passo consumido
	var concluido = null;
	
	this.reiniciar = function () {
		
		passo = null;
		concluindo = false;
		empilhando = false;
		
		$state.go("inicio");
		
	};
	
	// este método é chamado pelo listener de troca
	// de estado da aplicação, e é responsável por
	// analisar a troca de view e autorizar ou não,
	// com base nas solicitações de empilhamento ou
	// conclusão que foram efetuadas pela view atual
	this.isTransicaoAutorizada = function (toState, toParams, fromState, fromParams) {
		
		if (concluindo) {
			
			// estamos concluindo um estado,
			// podemos permitir a operação
			
			concluindo = false;
			return null;
			
		} else {
			
			// higienização das variáveis de controle
			concluido = null;
			
		}
		
		if (empilhando) {
			empilhando = false;
			
			// estamos empilhando um estado,
			// portanto precisamos verificar se o
			// estado de destino é um produtor conhecido
			if (produtores[passo.produtor.estado] == null) {
				return "Produtor não configurado: '" + passo.produtor.estado + "'";
			}

			return null;
			
		}
		
		// estamos em uma transição desvinculada a pilha,
		// precisamos verificar se temos algum processo de
		// pilha em andamento
		if (passo == null) {
			
			// não temos um processo de pilha em andamento,
			// qualquer transição neste cenário é permitida
			return null;
			
		}
		
		// temos uma pilha, e uma transição desvinculada a ela,
		// que pode apenas ser permitida se for uma transição
		// para RAÍZ (caso em que resetaremos a pilha) ou para
		// views internas do passo atual
		
		// começaremos verificando se é uma transição para raíz
		if ($.inArray(toState.name, raizes) >= 0) {
			this.reiniciar();
			return null;
		}
		
		// depois verificamos se é uma transição interna, ou
		// uma transição para a própria tela principal do produtor
		var permitidasDoProdutor = produtores[passo.produtor.estado].transicoesPermitidas;
		if ($.inArray(toState.name, permitidasDoProdutor) >= 0) {
			return null;
		}
		
		// transição não autorizada
		return "Transição fora da pilha não autorizada! (de: " + fromState.name + "; para: " + toState.name + "; passo atual: " + passo.origem.estado + " -> " + passo.produtor.estado + ")";
	};
	
	// deve ser chamado sempre que uma tela
	// quiser iniciar ou adicionar um passo
	// em um empilhamento. O objeto produtor
	// tem a seguinte estrutura:
	// {
	//   estado, 
	//   parametros, 
	//   opcoes
	// }
	// o terceiro parâmetro, "descricaoDoPasso",
	// serve como um texto a ser apresentado
	// na migalha da funcionalidade produtora
	this.irPara = function (produtor, $scope, descricaoDoPasso) {
		
		// armazena como primeiro passo ou
		// encadeia o passo atual como anterior
		// a este, mantendo a stack
		var anterior = passo;
		
		passo = {
			origem: {
				estado: $state.current.name,
				parametros: angular.copy($stateParams)
			},
			produtor: produtor,
			$scope: $scope,
			anterior: anterior,
			descricao: descricaoDoPasso
		};
		
		empilhando = true;
		
		// se já não estamos na tela atual
		if (produtor.estado != $state.current.name) {
			// vamos para a nova tela
			$state.go(produtor.estado, produtor.parametros || {});
		} else {
			// recarregamos a tela atual
			$state.reload(produtor.estado, produtor.parametros || {});
		}
		
	};
	
	// deve ser chamado sempre que uma funcionalidade que
	// potencialmente está sendo chamada de forma empilhada
	// termina sua função. caso ela de fato tenha sido chamada
	// de forma empilhada, esse método redireciona o usuário
	// novamente para o passo anterior, repassando eventuais valores
	// de retorno para os respectivos callbacks, se aplicável
	this.retornar = function (valor) {
		
		// verifica se existe algum passo pendente
		// pois pode ser que essa funcionalidade tenha sido
		// chamada diretamente do menu ou diretamente pela URL
		if (passo == null) {
			return false;
		}
		else
		{
			
			// existe uma chamada pendente, indicando que essa
			// tela foi chamada de forma empilhada (através de
			// outra funcionalidade)
			concluindo = true;
			concluido = passo;
			concluido.valor = valor;
			passo = concluido.anterior;
			
			// se a tela anterior não é a mesma que a atual,
			// retornamos via $state.go, caso contrário,
			// simplesmente recarregamos com $state.reload
			if (concluido.origem.estado != $state.current.name) {
				$state.go(concluido.origem.estado, concluido.origem.parametros);
			} else {
				$state.reload(concluido.origem.estado, concluido.origem.parametros);
			}
			
			return true;
		}
		
	};
	
	// permite recuperar uma opção de configuração
	// do passo atual da pilha, ou retorna o valor
	// padrão se um passo deste não existir
	this.getOpcao = function (chave, padrao) {		
		return (passo != null && passo.produtor["opcoes"] != null) ? passo.produtor.opcoes[chave] : padrao;
	};
	
	// permite recuperar a descrição do passo
	// atualmente empilhado
	this.getDescricaoDoPasso = function () {
		return passo != null ? passo.descricao : "";
	};
	
	// este método deve ser chamado pelas telas que
	// consomem funcionalidade de pilha, de modo
	// a recuperarem o $scope anterior e processarem
	// o valor retornado pela tela consumida. uma
	// função de callback, recebendo o objeto
	// do produtor que concluiu e o valor retornado
	// pode ser passada, opcionalmente, para este método
	this.recuperar = function ($scope, callback) {
		if (concluido == null) {
			return false;
		}
		else
		{
			
			// transporta os dados necessários de um escopo
			// empilhado para um escopo novo. um tratamento
			// especial é feito para evitar o transporte de
			// propriedades nativas do angularjs ou funções
			for (var k in concluido.$scope) {
				if (typeof concluido.$scope[k] !== 'function' && !k.startsWith("$")) {
					$scope[k] = concluido.$scope[k];
				}
			}
			
			if (callback) {
				callback(concluido.produtor, concluido.valor);
			}
			
			return true;
		}
	};
	
}]);

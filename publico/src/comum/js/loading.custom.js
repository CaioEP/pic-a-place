'use strict';

angular.module('app').factory('CustomLoading', function () {

    var $conteudo = $('#conteudo');
    var $loading = $('.loading-spiner-holder');
    return {
        ativar: function () {
            $conteudo.css('cursor', 'wait');
            $conteudo.css('pointer-events', 'none');
            $loading.show();
        },

        desativar: function () {
            $conteudo.css('-webkit-filter', 'blur(0px)');
            $conteudo.css('cursor', 'default');
            $conteudo.css('pointer-events', 'auto');
            $loading.hide();
        }



    };

});
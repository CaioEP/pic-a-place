var diretoriosInclusos = ['src/app/**/*.js', 'src/directives/**/*.js', 'src/helpers/**/*.js', 'src/comum/js/*.js', 'src/components/**/*.js', 'src/compartilhado/**/*.js'];
var diretoriosInclusosLogin = ['src/login/**/*.js', 'src/directives/*.js', 'src/helpers/**/*.js', 'src/compartilhado/**/*.js'];
var nomeArquivoApp = 'app';
var nomeArquivoLogin = 'login';

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    //foreach = require('gulp-foreach'),
    //notify = require('gulp-notify'),
    del = require('del'),
    //path = require('path'),
    util = require('gulp-util');

gulp.task('clean', function () {
    return del(['src/dist/'+nomeArquivoApp+'.dist?(.min).js']);
});
gulp.task('clean-login', function () {
    return del(['src/dist/'+nomeArquivoLogin+'.dist?(.min).js']);
});

gulp.task('scripts-app', function () {

    return gulp.src(diretoriosInclusos)
        .pipe(concat('dist/'+nomeArquivoApp + '.dist.js'))
        .pipe(gulp.dest('src'))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('src'))

});
gulp.task('scripts-login', function () {

    return gulp.src(diretoriosInclusosLogin)
        .pipe(concat('dist/'+nomeArquivoLogin + '.dist.js'))
        .pipe(gulp.dest('src'))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('src'))

});

gulp.task('watch', function () {
    gulp.watch(diretoriosInclusos, ['scripts-app']);
    gulp.watch(diretoriosInclusosLogin, ['scripts-login']);
});


gulp.task('build', ['clean', 'scripts-app', 'clean-login','scripts-login'], function () {});
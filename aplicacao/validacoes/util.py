class Util:
    @staticmethod
    def validar_cpfcnpj(cpfcnpj):
        tamanho = len(cpfcnpj)
        if tamanho == 11:
            return Util.validar_cpf(cpfcnpj)
        elif tamanho == 14:
            return Util.validar_cnpj(cpfcnpj)
        else:
            return False

    @staticmethod
    def validar_cpf(cpf):
        iguais = ["00000000000", "11111111111", "22222222222", "33333333333", "44444444444", "55555555555",
                  "66666666666", "77777777777", "88888888888", "99999999999"]

        if not cpf or (cpf in iguais):
            return False

        d1 = d2 = 0
        for count in range(1, len(cpf) - 1):
            digito_cpf = int(cpf[count - 1: count])
            d1 += (11 - count) * digito_cpf
            d2 += (12 - count) * digito_cpf
        resto = d1 % 11

        if resto < 2:
            digito1 = 0
        else:
            digito1 = 11 - resto

        d2 += 2 * digito1

        resto = d2 % 11

        if resto < 2:
            digito2 = 0
        else:
            digito2 = 11 - resto

        dig_verific = cpf[len(cpf) - 2: len(cpf)]
        dig_result = str(digito1) + str(digito2)

        return dig_verific == dig_result

    @staticmethod
    def validar_cnpj(cnpj):
        iguais = ["00000000000000", "11111111111111", "22222222222222", "33333333333333", "44444444444444",
                  "55555555555555", "66666666666666", "77777777777777", "88888888888888", "99999999999999"]

        if cnpj in iguais:
            return False

        soma = 0
        cnpj_calc = cnpj[0:12]

        char_cnpj = list(cnpj)

        for i in range(0, 4):
            if (int(char_cnpj[i]) >= 0) and (int(char_cnpj[i]) <= 9):
                soma += (int(char_cnpj[i])) * (6 - (i + 1))
        for i in range(0, 8):
            if (int(char_cnpj[i + 4]) >= 0) and (int(char_cnpj[i + 4]) <= 9):
                soma += int(char_cnpj[i + 4]) * (10 - (i + 1))

        dig = 11 - (soma % 11)
        cnpj_calc += '0' if (dig == 10 or dig == 11) else str(dig)
        soma = 0

        for i in range(0, 5):
            if (int(char_cnpj[i]) >= 0) and (int(char_cnpj[i] + "") <= 9):
                soma += int((char_cnpj[i])) * (7 - (i + 1))
        for i in range(0, 8):
            if (int(char_cnpj[i + 5]) >= 0) and (int(char_cnpj[i + 5]) <= 9):
                soma += (int(char_cnpj[i + 5])) * (10 - (i + 1))

        dig = 11 - (soma % 11);
        cnpj_calc += '0' if (dig == 10 or dig == 11) else str(dig)

        return cnpj == cnpj_calc

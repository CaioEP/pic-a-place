from wtforms import Form, validators
from wtforms.fields import StringField


class LoginForm(Form):
    email = StringField(validators=[validators.input_required(), validators.email()])
    senha = StringField(validators=[validators.input_required()])

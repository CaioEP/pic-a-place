from wtforms import Form, validators
from wtforms.fields import StringField, IntegerField


class UsuarioForm(Form):
    nome = StringField(validators=[validators.input_required()])
    email = StringField(validators=[validators.input_required(), validators.email()])
    senha = StringField(validators=[validators.input_required()])
    repeticao_senha = StringField(validators=[validators.input_required()])

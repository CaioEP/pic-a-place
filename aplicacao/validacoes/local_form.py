from wtforms import Form, validators
from wtforms.fields import StringField, IntegerField


class LocalForm(Form):
    nome = StringField(validators=[validators.input_required()])
    id_tipo_propriedade = IntegerField(validators=[validators.input_required()])

from flask import Flask, Session
from datetime import timedelta
from lib.models.base import setup
from .encoders.resultado_paginado_encoder import ResultadoPaginadoEncoder

import configparser, itertools
import wtforms_json

app = Flask(__name__)

config = configparser.ConfigParser()

filename = '../desenvolvimento.ini'
with open(filename) as fp:
    config.read_file(itertools.chain(['[DEFAULT]'], fp), source=filename)

# config.read("../desenvolvimento.ini")

setup(dict(config.items("DEFAULT")))

# Configuração da sessão
app.secret_key = 'super secret key'
app.config['SESSION_TYPE'] = 'memcached'
app.permanent_session_lifetime = timedelta(hours=2)
session = Session()

wtforms_json.init()

# imports das APIs

import aplicacao.api.usuario_api
import aplicacao.api.login_api
import aplicacao.api.local_api

# json encoders customizados
app.json_encoder = ResultadoPaginadoEncoder

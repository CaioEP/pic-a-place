from flask import jsonify, session

from aplicacao.decorators.acesso_restrito import acesso_restrito
from aplicacao import app
from aplicacao.decorators.parametro_json import parametrojson
from aplicacao.decorators.validador import validador
from aplicacao.validacoes.local_form import LocalForm
from lib.exceptions.negocio_exception import NegocioException
from lib.models.base import transacional, fecha_sessao
from lib.models.tipo_propriedade import TipoPropriedade
from lib.models.local import Local
from lib.filtros.paginacao import Paginacao
from lib.models.usuario import Usuario
from validacoes.local_form import LocalForm
from decorators.errorhandler import errorhandler


@app.route("/api/local/salvar", methods=['POST'], endpoint="inserir_local")
@errorhandler
@validador(LocalForm)
@transacional
@acesso_restrito('TODOS')
def inserir(json_param=None):
    local = Local(json_param)

    usuario_logado = session.get('usuario_logado')
    local.id_usuario = usuario_logado.get('id')
    local.usuario = Usuario.buscar_por_id(usuario_logado.get('id'))
    local.tipo_propriedade = TipoPropriedade.buscar_por_id(local.id_tipo_propriedade)

    local.salvar()

    return jsonify({
        'nome': local.nome,
        'valor_entrada': local.valor_entrada,
        'latitude': local.latitude,
        'longitude': local.longitude,
        'id_usuario': local.id_usuario,
        'id_tipo_propriedade': local.id_tipo_propriedade
    })


@app.route("/api/local/alterar/<int:id>", methods=['PUT'], endpoint="alterar_local")
@errorhandler
@validador(LocalForm)
@transacional
@acesso_restrito('TODOS')
def alterar(id, json_param=None):
    local = Local(json_param)
    usuario_logado = session.get('usuario_logado')
    local.id_usuario = usuario_logado.get('id')
    local.usuario = Usuario.buscar_por_id(usuario_logado.get('id'))
    local.tipo_propriedade = TipoPropriedade.buscar_por_id(local.id_tipo_propriedade)

    local.alterar()

    return jsonify({
        'nome': local.nome,
        'valor_entrada': local.valor_entrada,
        'latitude': local.latitude,
        'longitude': local.longitude,
        'id_usuario': local.id_usuario,
        'id_tipo_propriedade': local.id_tipo_propriedade
    })


@app.route("/api/local/<int:id>", methods=['DELETE'], endpoint="excluir_local")
@errorhandler
@transacional
@acesso_restrito('TODOS')
def excluir(id):
    local = Local.buscar_por_id(id)
    local.excluir()

    return ""


@app.route("/api/local/tipos_propriedade", methods=['GET'], endpoint="buscar_tipos_propriedade")
@fecha_sessao
def buscar_tipos_propriedade():
    tipos = TipoPropriedade.converter_para_lista(TipoPropriedade.todos())
    return jsonify(tipos)


@app.route("/api/local", methods=['GET'], endpoint="consultar_locais")
@parametrojson
@fecha_sessao
@acesso_restrito('TODOS')
def consultar(json_param=None):
    filtro = Paginacao(json_param.get('pagina'), json_param.get('tamanhoDaPagina'))
    locais = Local.consultar(filtro)

    return jsonify(locais)


@app.route("/api/local/<int:id>", methods=['GET'], endpoint="buscar_local_por_id")
@fecha_sessao
@acesso_restrito('TODOS')
def buscar_por_id(id):
    local = Local.buscar_por_id(id)

    return jsonify({
        "id": local.id,
        "nome": local.nome,
        "latitude": float(local.latitude),
        "longitude": float(local.longitude),
        "valor_entrada": str(local.valor_entrada),
        "id_usuario": local.id_usuario,
        "id_tipo_propriedade": local.id_tipo_propriedade
    })

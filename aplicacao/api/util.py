from flask import Response
import re
import mimetypes
import hashlib
import os


def monta_response_range(range_header, file_url, nome_video):
    size = os.path.getsize(file_url)
    byte_inicio, byte_fim = 0, None

    m = re.search('(\d+)-(\d*)', range_header)
    g = m.groups()

    if g[0]:
        byte_inicio = int(g[0])
    if g[1]:
        byte_fim = int(g[1])

    length = size - byte_inicio
    if byte_fim is not None:
        length = byte_fim - byte_inicio

    with open(file_url, 'rb') as f:
        f.seek(byte_inicio)
        data = f.read(length)

    hash_md5 = hashlib.md5()
    hash_md5.update(nome_video.encode('utf-8'))

    resp = Response(data, 206, mimetype=mimetypes.guess_type(file_url)[0], direct_passthrough=True)
    resp.headers.add('Content-Range', 'bytes {0}-{1}/{2}'.format(byte_inicio, byte_inicio + length - 1, size))
    resp.headers.add('Accept-Ranges', 'bytes')

    resp.headers.add('ETag', hash_md5.hexdigest())

    return resp

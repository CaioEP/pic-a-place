from flask import jsonify, session

from aplicacao import app
from aplicacao.decorators.validador import validador
from aplicacao.validacoes.login_form import LoginForm
from lib.exceptions.negocio_exception import NegocioException
from lib.models.base import transacional
from lib.models.usuario import Usuario
from decorators.errorhandler import errorhandler


@app.route("/api/login", methods=['POST'], endpoint="login")
@errorhandler
@validador(LoginForm)
@transacional
def entrar(json_param=None):
    session.clear()
    email = json_param.get('email')
    senha = json_param.get('senha')

    usuario = Usuario.buscar_por_email_senha(email, senha)
    if not usuario:
        raise NegocioException("Email e/ou Senha inválidos")

    usuario_logado = {
        'id': usuario.id,
        'nome': usuario.nome,
        'email': usuario.email,
        'id_perfil': usuario.id_perfil
    }

    session['usuario_logado'] = usuario_logado

    return jsonify({
        'nome': usuario.nome,
        'email': usuario.email,
        'id_perfil': usuario.id_perfil,
        'perfil': usuario.perfil.descricao
    })


@app.route('/api/logout', methods=['GET'], endpoint='sair')
@errorhandler
@transacional
def sair():
    session.clear()
    return ""


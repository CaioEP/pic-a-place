from flask import jsonify, session

from aplicacao.decorators.acesso_restrito import acesso_restrito
from aplicacao import app
from aplicacao.decorators.parametro_json import parametrojson
from aplicacao.decorators.validador import validador
from aplicacao.validacoes.usuario_form import UsuarioForm
from lib.exceptions.negocio_exception import NegocioException
from lib.models.base import transacional, fecha_sessao
from lib.models.perfil import Perfil
from lib.models.usuario import Usuario
from lib.filtros.filtro_usuario import FiltroUsuario
from decorators.errorhandler import errorhandler


@app.route("/api/usuario/salvar", methods=['POST'], endpoint="inserir_usuario")
@errorhandler
@validador(UsuarioForm)
@transacional
@acesso_restrito('USUARIO_CADASTRAR')
def inserir(json_param=None):
    usuario = Usuario(json_param)
    usuario.id_perfil = usuario.id_perfil if usuario.id_perfil else usuario.id_perfil
    usuario.perfil = Perfil.buscar_por_id(usuario.id_perfil)

    if not Usuario.buscar_por_email(usuario.email):
        usuario.salvar()
    else:
        raise NegocioException('Já existe este Login na base de dados')

    return jsonify({
        'nome': usuario.nome,
        'email': usuario.email,
        'senha': usuario.senha,  # retornando a senha pois é necessário para efetuar o login diretamente após cadastro
        'perfil': usuario.perfil.descricao
    })


@app.route("/api/usuario/alterar/<int:id>", methods=['PUT'], endpoint="alterar_usuario")
@errorhandler
@validador(UsuarioForm)
@transacional
@acesso_restrito('USUARIO_ALTERAR')
def alterar(id, json_param=None):
    usuario = Usuario(json_param)

    usuario_logado = session.get('usuario_logado')
    usuario_verificado = Usuario.buscar_por_id_email(id, usuario.email)
    if not usuario_verificado:
        usuario.senha = usuario.senha
        usuario.alterar()
        if usuario_logado and int(usuario_logado.get('id')) == usuario.id:
            renovar_session(usuario)
    else:
        raise NegocioException('Este email já está em uso!')

    return jsonify({
        'nome': usuario.nome,
        'email': usuario.email,
        'senha': usuario.senha,
        'perfil': usuario.id_perfil
    })


@app.route("/api/usuario/por_login", methods=['GET'], endpoint="buscar_usuario_por_login")
@parametrojson
@fecha_sessao
@acesso_restrito('USUARIO_CONSULTAR')
def buscar_por_login(json_param=None):
    login = json_param.get('login')
    usuario = Usuario.buscar_por_email(login)

    return jsonify(usuario.converter_para_dict()) if usuario else ""


@app.route("/api/usuario/<int:id>", methods=['GET'], endpoint="buscar_usuario_por_id")
@fecha_sessao
@acesso_restrito('USUARIO_CONSULTAR')
def buscar_por_id(id):
    usuario = Usuario.buscar_por_id(id)

    return jsonify(usuario.converter_para_dict()) if usuario else ""


@app.route("/api/usuario/perfis", methods=['GET'], endpoint="buscar_todos_perfis")
@fecha_sessao
def buscar_todos_perfis():
    perfis = Perfil.converter_para_lista(Perfil.todos())
    return jsonify(perfis)


@app.route("/api/usuario", methods=['GET'], endpoint="consultar_usuarios")
@parametrojson
@fecha_sessao
@acesso_restrito('USUARIO_CONSULTAR')
def consultar(json_param=None):
    filtro = FiltroUsuario(json_param.get('pagina'), json_param.get('tamanhoDaPagina'))
    filtro.nome = json_param.get('nome')
    filtro.email = json_param.get('email')
    filtro.id_perfil = json_param.get('id_perfil')
    usuarios = Usuario.consultar(filtro)

    return jsonify(usuarios)


@app.route("/api/usuario/<int:id>", methods=['DELETE'], endpoint="excluir_usuario")
@errorhandler
@transacional
@acesso_restrito('USUARIO_EXCLUIR')
def excluir(id):
    usuario = Usuario.buscar_por_id(id)
    id_usuario_logado = session.get('usuario_logado').get('id') if session.get('usuario_logado') else None
    usuario.excluir()

    # Se o usuário logado se excluiu do sistema, limpa a sessão
    usuarios_iguais = id_usuario_logado and id == id_usuario_logado
    if usuarios_iguais:
        session.clear()

    return jsonify(usuarios_iguais)


@app.route("/api/usuario/resetar-senha/<int:id>", methods=['PUT'], endpoint="resetar_senha")
@errorhandler
@transacional
@acesso_restrito('USUARIO_ALTERAR')
def resetar_senha(id):
    usuario_logado = session.get('usuario_logado')

    usuario = Usuario.buscar_por_id(id)
    usuario.data_ultimo_login = None
    usuario.alterar()

    return jsonify({
        'nome': usuario.nome,
        'login': usuario.login,
        'senha': usuario.senha,
        'logado': usuario.id == usuario_logado.get('id')
    })


@app.route("/api/usuario/roles", methods=['GET'], endpoint="buscar_roles")
@fecha_sessao
def buscar_roles():
    usuario_logado = session.get('usuario_logado')

    if usuario_logado:
        id_usuario = usuario_logado.get('id')
        id_perfil = usuario_logado.get('id_perfil')

        roles = Usuario.buscar_roles(id_usuario, id_perfil)
        return jsonify(roles)
    return ""


def renovar_session(usuario):
    usuario_logado = {
        'id': usuario.id,
        'nome': usuario.nome,
        'email': usuario.email,
        'id_perfil': usuario.id_perfil,
    }
    session.clear()
    session['usuario_logado'] = usuario_logado

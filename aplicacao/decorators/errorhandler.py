from flask import json, Response


def errorhandler(fn):

    def wrapper_error(*arg, **kw):
        try:
            result = fn(*arg, **kw)
        except Exception as ex:
            response = Response(json.dumps({
                    "errors": str(ex),
                    "tipo_erro": "erro_de_negocio"
                }),
                status=400,
                mimetype='application/json')
            return response
        else:
            return result

    return wrapper_error

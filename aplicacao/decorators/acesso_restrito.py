from flask import json, session, Response
from functools import wraps
from aplicacao import app
from lib.models.usuario import Usuario


def acesso_restrito(role):

    def wrap(fn):
        @wraps(fn)
        def wrapper_autenticacao(*arg, **kw):

            sessao_logado = session.get('usuario_logado')

            if app.debug:
                print('%s' % role)
                print('%s' % sessao_logado)

            if not sessao_logado:
                response = Response(json.dumps({
                    "errors": 'É necessário entrar no sistema para continuar',
                    "tipo_erro": 'erro_de_autenticacao'
                }),
                    status=401,
                    mimetype='application/json')
                return response

            # controle de permissão aqui
            if role != 'TODOS':
                tem_acesso = Usuario.verificar_role(role, sessao_logado.get('id'))
                if not tem_acesso:
                    response = Response(json.dumps({
                        "errors": 'Você não tem permissão para acessar a funcionalidade',
                        "tipo_erro": 'erro_de_permissao'
                    }),
                        status=403,
                        mimetype='application/json')
                    return response

            return fn(*arg, **kw)

        return wrapper_autenticacao

    return wrap

from flask import request, json
from aplicacao import app


def parametrojson(fn):

    def wrapper_json(*arg, **kw):

        try:
            json_param = request.get_json()
        except Exception as e:
            print(e)

        file = request.files.get('file')
        if not json_param:
            json_param = {}
        if file:
            json_param['file'] = file

        # se não tivermos valor no request payload, tentamos recupera da queryString
        if not json_param:
            json_param = request.args

        if app.debug:
            print('parâmetros requisição: ')
            print(json.dumps(json_param))

        # recupera o json da requisição e adiciona nos parâmetros nomeados
        kw["json_param"] = json_param

        return fn(*arg, **kw)
    return wrapper_json

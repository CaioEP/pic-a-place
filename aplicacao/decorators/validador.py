from flask import request, json, Response

from aplicacao import app


def validador(form_config):

    def wrap(fn):
        def wrapper_validador(*arg, **kw):

            print(request)
            json_param = request.get_json()

            if app.debug:
                print('parâmetros requisição: ')
                print(json.dumps(json_param))

            form = form_config.from_json(json_param)
            if not form.validate():
                response = Response(json.dumps({
                                        "errors": form.errors,
                                        "tipo_erro": "erro_de_validacao"
                                    }),
                                    status=400,
                                    mimetype='application/json')
                return response

            # recupera o json da requisição e adiciona nos parâmetros nomeados
            kw["json_param"] = json_param

            return fn(*arg, **kw)
        return wrapper_validador
    return wrap

from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from lib.models.base import Session, ModelBase
from .base import Base
from .perfil import Perfil
from .perfil_permissao import PerfilPermissao
from .permissao import Permissao
from .resultado_paginado import ResultadoPaginado


class Usuario(Base, ModelBase):
    __tablename__ = 'usuario'

    id = Column(Integer, primary_key=True, autoincrement=True)
    nome = Column(String, nullable=False)
    email = Column(String, nullable=False)
    senha = Column(String, nullable=False)
    id_perfil = Column(Integer, ForeignKey('perfil.id'), nullable=False)
    perfil = relationship(Perfil)

    @classmethod
    def todos(cls):
        return Session.query(cls).all()

    @classmethod
    def buscar_por_email(cls, email):
        return Session.query(cls).filter_by(email=email).first()

    @classmethod
    def buscar_por_id(cls, id):
        return Session.query(cls).filter_by(id=id).first()

    @classmethod
    def buscar_por_id_email(cls, id, email):
        return Session.query(cls).filter(cls.id != id, cls.email == email).first()

    @classmethod
    def buscar_por_email_senha(cls, email, senha):
        return Session.query(cls).filter(cls.email == email, cls.senha == senha).first()

    @classmethod
    def consultar(cls, filtro):
        query = Session.query(cls).join(cls.perfil)
        if filtro.nome:
            query = query.filter(cls.nome.like('%' + filtro.nome + '%'))
        if filtro.email:
            query = query.filter(cls.email.like('%' + filtro.email + '%'))

        return ResultadoPaginado(query, filtro, cls.converter_para_lista)

    def salvar(self):
        Session.add(self)

    def alterar(self):
        Session.merge(self)

    def excluir(self):
        Session.delete(self)

    def __init__(self, json):
        self.preencher(json)

    def preencher(self, json):
        self.id = json.get('id', None)
        self.nome = json.get('nome', None)
        self.email = json.get('email', None)
        self.senha = json.get('senha', None)
        self.id_perfil = json.get('id_perfil', None)

    @staticmethod
    def converter_para_lista(dados):
        usuarios = []

        for item in dados:
            usuarios.append({
                "id": item.id,
                "nome": item.nome,
                "email": item.email,
                "senha": item.senha,
                "id_perfil": item.id_perfil,
                "descricao_perfil": item.perfil.descricao
            })

        return usuarios

    @staticmethod
    def verificar_role(role, id_usuario):
        query = Session.query(PerfilPermissao).\
            join(Perfil, Perfil.id == PerfilPermissao.id_perfil). \
            join(Permissao, Permissao.id == PerfilPermissao.id_permissao).\
            join(Usuario, Usuario.id_perfil == Perfil.id).\
            filter(Usuario.id == id_usuario).\
            filter(Permissao.role == role).\
            limit(1)

        resultado = query.first()

        return True if resultado else False

    @staticmethod
    def buscar_roles(id_usuario, id_perfil):
        query = Session.query(Permissao.role).\
            select_from(Permissao).\
            join(PerfilPermissao, PerfilPermissao.id_permissao == Permissao.id).\
            join(Perfil, Perfil.id == PerfilPermissao.id_perfil).\
            join(Usuario, Usuario.id_perfil == Perfil.id).\
            filter(Perfil.id == id_perfil).\
            filter(Usuario.id == id_usuario)

        resultado = query.all()

        roles = []
        for role in resultado:
            roles.append(role.role)

        return roles if roles else None

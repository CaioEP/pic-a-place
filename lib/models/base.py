from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import engine_from_config
from sqlalchemy.pool import QueuePool

from lib.exceptions.negocio_exception import NegocioException

from decorator import decorator


engine = None


def setup(config):
    """Setup the application given a config dictionary."""

    global engine
    engine = engine_from_config(config, "sqlalchemy.", poolclass=QueuePool)
    Session.configure(bind=engine)


@decorator
def transacional(fn, *arg, **kw):
    """Decorate any function to commit the session on success, rollback in
    the case of error."""

    try:
        result = fn(*arg, **kw)
        Session.commit()
    except Exception as e:
        Session.rollback()

        if hasattr(e, 'orig'):
            orig = e.orig
            if orig:
                if orig.errno and orig.errno == 1451:
                    msg = 'O registro não pode ser excluído pois está vinculado a outras informações no sistema'
                    raise NegocioException(msg)
        raise
    else:
        return result


@decorator
def fecha_sessao(fn, *arg, **kw):
    try:
        result = fn(*arg, **kw)
    except Exception as e:
        raise
    else:
        return result
    finally:
        Session.close()


# bind the Session to the current request
Session = scoped_session(sessionmaker())

Base = declarative_base()


class ModelBase:

    def converter_para_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

# establish a constraint naming convention.
# see http://docs.sqlalchemy.org/en/latest/core/constraints.html#configuring-constraint-naming-conventions
#
# Base.metadata.naming_convention={
#         "pk": "pk_%(table_name)s",
#         "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
#         "uq": "uq_%(table_name)s_%(column_0_name)s",
#         "ix": "ix_%(table_name)s_%(column_0_name)s"
#     }

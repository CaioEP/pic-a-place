from sqlalchemy import Column, Integer, String
from lib.models.base import Session, ModelBase
from .base import Base


class Perfil(Base, ModelBase):
    __tablename__ = 'perfil'

    id = Column(Integer, primary_key=True, autoincrement=True)
    descricao = Column(String, nullable=False)

    @classmethod
    def todos(cls):
        return Session.query(cls).all()

    @classmethod
    def buscar_por_id(cls, id):
        return Session.query(cls).filter_by(id=id).first()

    def salvar(self):
        Session.add(self)

    def __init__(self, json):
        self.preencher(json)

    def preencher(self, json):
        self.id = json.get('id', None)
        self.descricao = json.get('descricao')

    @staticmethod
    def converter_para_lista(dados):
        perfil = []

        for item in dados:
            perfil.append(item.converter_para_dict())

        return perfil

from sqlalchemy import Column, Integer, String
from lib.models.base import Session, ModelBase
from .base import Base


class TipoPropriedade(Base, ModelBase):
    __tablename__ = 'tipo_propriedade'

    id = Column(Integer, primary_key=True, autoincrement=True)
    descricao = Column(String, nullable=False)

    @classmethod
    def todos(cls):
        return Session.query(cls).all()

    @classmethod
    def buscar_por_id(cls, id):
        return Session.query(cls).filter_by(id=id).first()

    def salvar(self):
        Session.add(self)

    def alterar(self):
        Session.merge(self)

    def excluir(self):
        Session.delete(self)

    def __init__(self, json):
        self.preencher(json)

    def preencher(self, json):
        self.id = json.get('id', None)
        self.descricao = json.get('descricao', None)

    @staticmethod
    def converter_para_lista(dados):
        tipos = []

        for item in dados:
            tipos.append({
                "id": item.id,
                "descricao": item.descricao
            })

        return tipos

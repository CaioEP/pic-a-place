from sqlalchemy import Column, Integer, ForeignKey, String
from sqlalchemy.orm import relationship
from lib.models.base import Session, ModelBase
from .base import Base
from .perfil import Perfil
from .permissao import Permissao


class PerfilPermissao(Base, ModelBase):
    __tablename__ = 'perfil_permissao'

    id_perfil = Column(Integer, ForeignKey('perfil.id'), primary_key=True)
    id_permissao = Column(Integer, ForeignKey('permissao.id'), primary_key=True)

    perfil = relationship(Perfil)
    permissao = relationship(Permissao)

    @classmethod
    def todas(cls):
        return Session.query(cls).all()

    def salvar(self):
        Session.add(self)

    def alterar(self):
        Session.merge(self)

    def __init__(self, json):
        self.preencher(json)

    def preencher(self, json):
        self.id_perfil = json.get('id_perfil', None)
        self.id_permissao = json.get('id_permissao', None)

    @staticmethod
    def converter_para_lista(dados):
        perfilpermissao = []

        for item in dados:
            perfilpermissao.append(item.converter_para_dict())

        return perfilpermissao

from enum import Enum


class PerfilEnum(Enum):
    administrador = 1
    comum = 2

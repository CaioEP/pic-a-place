from sqlalchemy import Column, Integer, ForeignKey, String
from sqlalchemy.orm import relationship
from lib.models.base import Session, ModelBase
from .base import Base
from .funcionalidade import Funcionalidade
from .acao import Acao


class Permissao(Base, ModelBase):
    __tablename__ = 'permissao'

    id = Column(Integer, primary_key=True, autoincrement=True)
    id_funcionalidade = Column(Integer, ForeignKey('funcionalidade.id'))
    id_acao = Column(Integer, ForeignKey('acao.id'))
    role = Column(String, nullable=False)

    funcionalidade = relationship(Funcionalidade)
    acao = relationship(Acao)

    @classmethod
    def todas(cls):
        return Session.query(cls).all()

    def salvar(self):
        Session.add(self)

    def alterar(self):
        Session.merge(self)

    def __init__(self, json):
        self.preencher(json)

    def preencher(self, json):
        self.id = json.get('id', None)
        self.id_acao = json.get('id_acao', None)
        self.id_funcionalidade = json.get('id_funcionalidade', None)
        self.role = json.get('role', None)

    @staticmethod
    def converter_para_lista(dados):
        impressoes = []

        for item in dados:
            impressoes.append(item.converter_para_dict())

        return impressoes

from sqlalchemy import Column, Integer, String
from lib.models.base import Session, ModelBase
from .base import Base


class Acao(Base, ModelBase):
    __tablename__ = 'acao'

    id = Column(Integer, primary_key=True, autoincrement=True)
    descricao = Column(String, nullable=False)

    @classmethod
    def todos(cls):
        return Session.query(cls).all()

    def salvar(self):
        Session.add(self)

    def __init__(self, json):
        self.preencher(json)

    def preencher(self, json):
        self.id = json.get('id', None)
        self.descricao = json.get('descricao', None)

    @staticmethod
    def converter_para_lista(dados):
        acao = []

        for item in dados:
            acao.append(item.converter_para_dict())

        return acao

from sqlalchemy import Column, Integer, String, ForeignKey, DECIMAL
from sqlalchemy.orm import relationship
from lib.models.base import Session, ModelBase
from .base import Base
from .usuario import Usuario
from .tipo_propriedade import TipoPropriedade
from .resultado_paginado import ResultadoPaginado


class Local(Base, ModelBase):
    __tablename__ = 'local'

    id = Column(Integer, primary_key=True, autoincrement=True)
    nome = Column(String, nullable=False)
    latitude = Column(DECIMAL(precision=5, scale=8), nullable=False)
    longitude = Column(DECIMAL(precision=5, scale=8), nullable=False)
    valor_entrada = Column(DECIMAL(precision=15, scale=2), nullable=True)
    id_usuario = Column(Integer, ForeignKey('usuario.id'), nullable=False)
    id_tipo_propriedade = Column(Integer, ForeignKey('tipo_propriedade.id'), nullable=False)
    usuario = relationship(Usuario)
    tipo_propriedade = relationship(TipoPropriedade)

    @classmethod
    def todos(cls):
        return Session.query(cls).all()

    @classmethod
    def consultar(cls, filtro):
        query = Session.query(cls)\
            .join(cls.usuario)\
            .join(cls.tipo_propriedade)
        return ResultadoPaginado(query, filtro, cls.converter_para_lista)

    @classmethod
    def buscar_por_id(cls, id):
        return Session.query(cls).filter_by(id=id).first()

    def salvar(self):
        Session.add(self)

    def alterar(self):
        Session.merge(self)

    def excluir(self):
        Session.delete(self)

    def __init__(self, json):
        self.preencher(json)

    def preencher(self, json):
        self.id = json.get('id', None)
        self.nome = json.get('nome', None)
        self.latitude = json.get('latitude', None)
        self.longitude = json.get('longitude', None)
        self.valor_entrada = json.get('valor_entrada', None)
        self.id_usuario = json.get('id_usuario', None)
        self.id_tipo_propriedade = json.get('id_tipo_propriedade', None)

    @staticmethod
    def converter_para_lista(dados):
        local = []

        for item in dados:
            local.append({
                "id": item.id,
                "nome": item.nome,
                "latitude": str(item.latitude),
                "longitude": str(item.longitude),
                "valor_entrada": str(item.valor_entrada),
                "id_usuario": item.id_usuario,
                "id_tipo_propriedade": item.id_tipo_propriedade,
                "descricao_tipo_propriedade": item.tipo_propriedade.descricao,
                "url_foto": 'https://cptstatic.s3.amazonaws.com/imagens/enviadas/materias/materia10372/3criacao-de-capivaras-curso-cpt.jpg'
            })

        return local
